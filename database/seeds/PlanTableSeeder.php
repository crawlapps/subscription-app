<?php

use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            [
                'id' => 1,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "Basic"),
                'price' => env('PLAN_PRICE_1', 10),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Basic"),
                'trial_days' => env('TRIAL_DAY', 30),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1)
            ],
            [
                'id' => 2,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "Advanced"),
                'price' => env('PLAN_PRICE_1', 50),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Advanced"),
                'trial_days' => env('TRIAL_DAY', 30),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1)
            ],
        ]);
    }
}
