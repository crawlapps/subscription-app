<?php

use App\Models\State;
use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = ['Andaman and Nicobar Islands', 'Andhra Pradesh','Arunachal Pradesh', 'Assam','Bihar', 'Chandigarh','Chhattisgarh', 'Dadra and Nagar Haveli','Daman and Diu', 'Delhi','Goa', 'Gujarat','Haryana', 'Himachal Pradesh','Jammu and Kashmir', 'Jharkhand','Karnataka', 'Kerala','Ladakh', 'Lakshadweep','Madhya Pradesh', 'Maharashtra','Manipur', 'Meghalaya','Mizoram', 'Nagaland','Odisha', 'Puducherry','Punjab', 'Rajasthan','Sikkim', 'Tamil Nadu','Telangana', 'Tripura','Uttar Pradesh', 'Uttarakhand','West Bengal'];
        foreach ($states as $key=>$val){
            State::updateOrcreate([
                'name' => $val,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ], [
                'name' => $val,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
