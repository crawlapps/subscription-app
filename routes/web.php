<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shopify', 'check.plan'])->name('home');

Route::get(
    '/login', 'Auth\AuthController@index'
)->name('login');

Route::group(['middleware' => 'auth.shopify', 'namespace' => 'Plan'], function () {
    Route::get('/app-plan', 'PlanController@appPlanIndex')->name('app-plan');
    Route::get('/mbilling/{plan?}', 'PlanController@appPlanChange')->name('billing');
});

//event
Route::post('/event', 'Plan\PlanController@addEvent')->name('event');

Route::group(['middleware' => ['auth.shopify', 'check.plan']], function () {
    Route::get('/test', 'Test\TestController@test');
    //dashboard
    Route::group(['namespace' => 'Dashboard'], function () {
        Route::resource('dashboard', 'DashboardController');
        Route::post('replace-lineitems', 'DashboardController@replaceLineItem')->name('replace-lineitems');

    });

    //subscriber
    Route::group(['namespace' => 'Subscriber'], function () {
        Route::resource('subscriber', 'SubscriberController');
        Route::get('subscribers/export/{type}/{s?}', 'SubscriberController@export');
        Route::post('save-comment', 'SubscriberController@saveComment')->name('save-comment');
        Route::post('save-lineitems', 'SubscriberController@saveLineItem')->name('save-lineitems');
        Route::get('get-country', 'SubscriberController@getCountry')->name('get-country');
    });

    //plan
    Route::group(['namespace' => 'Plan'], function () {
        //plan group
        Route::get('/plan-group', 'PlanController@planGroupIndex')->name('plan-group');
        Route::post('/plan-group', 'PlanController@planGroupStore')->name('plan-group');
        Route::get('/plan-group.edit/{id?}', 'PlanController@planGroupEdit')->name('plan-group.edit');
        Route::delete('/plan-group.delete/{id?}', 'PlanController@planGroupDestroy')->name('plan-group.delete');

        // plan
        Route::post('/plan', 'PlanController@planStore')->name('plan.store');
        Route::get('/plan.edit/{planGid?}/{id?}', 'PlanController@planEdit')->name('plan.edit');
        Route::delete('/plan.delete/{id?}', 'PlanController@planDestroy')->name('plan.delete');

        //product
        Route::post('/assign-product', 'PlanController@assignProduct')->name('assign-product');

        //position
        Route::post('/position', 'PlanController@position')->name('position');

        //change plan
        Route::get('change-plan-db', 'PlanController@changePlanDB')->name('change-plan-db');
    });

       Route::get('/test', 'Test\TestController@test');

    //settings
    Route::group(['namespace' => 'Setting'], function () {
        Route::resource('setting', 'SettingController');

        //mail
        Route::post('/mail', 'SettingController@sendMail')->name('mail');
        Route::post('/email-body', 'SettingController@emailBody')->name('email-body');

        //images
        Route::post('/upload-image', 'SettingController@uploadImage')->name('upload-image');

        //customer portal
        Route::get('/portal-status', 'SettingController@changePortal')->name('portal-status');

        //theme install
        Route::post('/install-theme', 'SettingController@installTheme')->name('install-theme');
    });
});

//proxy
Route::group(['prefix' => 'portal', 'namespace' => 'Portal', 'middleware' => 'proxy.auth'], function () {
    // Route::get('/', function () {
    //     return Response()->view('portal.portal', [], 200)->withHeaders([
    //             'Content-Type'=>'application/liquid',
    //         ]);
    //     // return view('portal.portal');
    // });
    Route::get('/', 'PortalController@index');
    // Route::get('/subscriber', 'PortalController@subscriber')->name('portalsubscriber');
//    Route::get('/', 'PortalController@index')->name('portal.index');
//    router.get('/proxy', (req, res) => {
//        res.set('Content-Type', 'application/liquid').sendFile(path.join(__dirname, '../client/build/index.html'));
//    });

});

Route::get('flush', function(){
    request()->session()->flush();
});
