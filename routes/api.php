<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api','middleware' => ['cors']], function () {
	Route::post('subscriber', 'PortalController@index');
});
//proxy
// Route::group(['prefix' => 'portal', 'namespace' => 'Portal', 'middleware' => 'cors'], function () {
//     Route::post('/subscriber', 'PortalController@subscriber')->name('portalsubscriber');

// });

//Route::group([
//    'namespace' => 'Api',
//    'middleware' => 'api',
//    'prefix' => 'auth'
//
//], function ($router) {
//    Route::post('login', 'AuthController@login');
//    Route::post('register', 'AuthController@register');
//    Route::post('logout', 'AuthController@logout');
//    Route::post('refresh', 'AuthController@refresh');
//    Route::get('me', 'AuthController@me');
//});
