<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->ss_customer_id,
            'status' => $this->status,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'order_count' => $this->order_count,
            'phone' => $this->phone,
            'email' => $this->email,
            'date_first_order' => date("M d, Y", strtotime($this->date_first_order)),
            'next_order_date' => date("M d, Y", strtotime($this->next_order_date))
        ];
    }
}
