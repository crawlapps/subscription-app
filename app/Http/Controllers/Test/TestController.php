<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use App\Models\App;
use App\Models\ExchangeRate;
use App\Models\Shop;
use App\Models\SsSetting;
use App\Models\SsPlan;
use App\Models\Ss_customer;
use App\Models\SsContractLineItem;
use App\Models\SsDeletedProduct;
use App\Traits\ShopifyTrait;
use Illuminate\Http\Request;
use App\Models\SsOrder;
use Illuminate\Support\Facades\Auth;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Http;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;
use App\Models\Ss_contract;
use RuntimeException;
use Symfony\Component\Intl\Currencies;

class TestController extends Controller
{
    use ShopifyTrait;

    public function test(){
        // $this->funfillmentOrder();
        $user = Auth::user();
       $this->updateContract($user);
    
      // $this->billingAttempt();
        //   $endPoint = 'admin/api/' . env('SHOPIFY_SAPI_VERSION') . '/webhooks.json';
        //     $result = $user->api()->rest('GET', $endPoint, []);
        //     dd($result['body']->container);
        // $currentTime = date('Y-m-d H:i:s');
        // $subscriber = Ss_customer::select('id', 'first_name', 'last_name', 'active', 'phone', 'email',
        //         'total_orders', 'total_spend', 'total_spend_currency')->where('shop_id', $shopID)
        //         ->where(function ($query) use ($s) {
        //             $query->where('first_name', 'LIKE', '%'.$s.'%')->orWhere('last_name', 'LIKE',
        //                 '%'.$s.'%')->orWhere('email', 'LIKE', '%'.$s.'%')->orWhere('phone', 'LIKE', '%'.$s.'%');
        //         })->orderBy('created_at', 'desc')->paginate(10)->toArray();

        // $db_contract = Ss_contract::where('user_id', 6)->where('status', 'active')->whereDate('next_processing_date', '<=', $currentTime)
        // ->where(function ($query) {
        //      $query->where('status_billing', '!=', 'pending')->orWhere('status_billing', NULL);
        // })->get();
        // dd($db_contract);
       // $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/orders/2889977036955.json';
       //  $parameter['fields'] = 'id,total_price';
       //  $result = $user->api()->rest('GET', $endPoint, $parameter);
       //  if( !$result['errors'] ){
       //      dd($result['body']->container['order']);
       //  }else{
       //      dd($result);
       //  }
       //   // $this->billingAttempt();
       //    $this->contractCreate();
    }

    public function funfillmentOrder(){
        $user = Auth::user();
        $last_billing_order = '2929888034971';
        $query = '{
                    order(id: "gid://shopify/Order/'. $last_billing_order .'") {
                      displayFulfillmentStatus
                      fulfillmentOrders(first: 10) {
                        edges {
                          node {
                            fulfillAt
                            status
                            id
                            order {
                              name
                              legacyResourceId
                            }
                          }
                        }
                      }
                    }
                  }';
         $result =  $this->graphQLRequest($user->id, $query);
         dd($result);        
    }
    public function customerPaymentMethods(){
        $user = Auth::user();
        $contractId = 4554907;
        $query = '{
                     subscriptionContract(id: "gid://shopify/SubscriptionContract/'. $contractId .'") {
                        customerPaymentMethod {
                          id
                        }
                    }
                }';

        $result =  $this->graphQLRequest($user->id, $query);
        if( !$result['errors'] ){
          $subContract = $result['body']->container['data']['subscriptionContract'];
          $paymentMethodID = (@$subContract['customerPaymentMethod']['id']) ? $subContract['customerPaymentMethod']['id'] : '';
          if( $paymentMethodID ){
              $paymentQuery = 'mutation{
                          customerPaymentMethodSendUpdateEmail(customerPaymentMethodId: "'.$paymentMethodID.'") {
                            customer {
                              id
                            }
                            userErrors {
                              field
                              message
                            }
                        }
                      }';   
               $result =  $this->graphQLRequest($user->id, $paymentQuery);   
               dd($result);    
          }
        }
        dd($result);
    }

    public function updateContract($user){
        $contractId = 5013659;

        // dd($this->getContract($user, $contractId));
         // get draft id from contract id
         $query = 'mutation {
         	subscriptionContractUpdate(contractId: "gid://shopify/SubscriptionContract/'.$contractId.'") {
			    draft {
			      id
			    }
			    userErrors {
		      	  message
			    }
			  }
         	}';
         $resultSubscriptionContract =  $this->graphQLRequest($user->id, $query);
         if( !$resultSubscriptionContract['errors'] ){
         	$subscriptionContract = $resultSubscriptionContract['body']->container['data']['subscriptionContractUpdate'];
         	if( empty($subscriptionContract['userErrors']) ){
         		$draftId = (@$subscriptionContract['draft']['id']) ? $subscriptionContract['draft']['id'] : '';
         		if( $draftId ){
         			 // $draftQuery = '
			          //   mutation{
			          //     subscriptionDraftUpdate(draftId: "'. $draftId .'", 
             //          input: {
             //              status: ACTIVE, 
             //              // nextBillingDate: "", 
             //              // billingPolicy: {interval: DAY, intervalCount: 10, anchors: {day: 10, month: 10, type: WEEKDAY}, maxCycles: 10, minCycles: 10}, 
             //              // deliveryMethod: {shipping: {address: {address1: "", address2: "", city: "", company: "", country: "", countryCode: AF, firstName: "", id: "", lastName: "", phone: "", province: "", provinceCode: "", zip: ""}}},
             //              // deliveryPolicy: {interval: DAY, intervalCount: 10, anchors: {day: 10, month: 10, type: WEEKDAY}}
             //          }) {
             //          userErrors {
             //            code
             //            field
             //            message
             //          }
			          //   }';
                // craete draft
                   $draftQuery = '
                        mutation{
                          subscriptionDraftUpdate(draftId: "'. $draftId .'", 
                            input: {
                                status: PAUSED
                            }) {
                            userErrors {
                              code
                              field
                              message
                            }
                             draft {
                              id
                            }
                        }
                      }';
               dump('subscriptionDraftUpdate');
    					$draftResult =  $this->graphQLRequest($user->id, $draftQuery);
              dump($draftResult);
              $draftID = $draftResult['body']->container['data']['subscriptionDraftUpdate']['draft']['id'];

              // commit draft
              $commitDraft = '
                  mutation{
                    subscriptionDraftCommit(draftId: "'.$draftID.'") {
                    userErrors {
                      code
                      field
                      message
                    }
                  }
                }';
                 dump('commitDraft');
                 dump($this->graphQLRequest($user->id, $commitDraft));    
              $updateContractQuery = 'mutation{
                          subscriptionContractUpdate(contractId: "gid://shopify/SubscriptionContract/'.$contractId.'") {
                            draft {
                              status
                              id
                            }
                          }
                        }';
                   dump('subscriptionContractUpdate');
                 dump($this->graphQLRequest($user->id, $updateContractQuery));    
                  dump('getContract');   
                dd($this->getContract($user, $contractId));
         		}
         	}
         }
    }

    public function getContract($user, $contractId){
      $query = '{
         subscriptionContract(id: "gid://shopify/SubscriptionContract/'. $contractId .'") {
            status
          }
      }';
      return $this->graphQLRequest($user->id, $query);
    }  
    public function billingAttemptJob(){
      try{
        $user = Auth::user();
        $shop = Shop::where('user_id', $user->id)->first();

        // $ssContract = Ss_contract::where('status', 'active')->where('status_billing', '!=', 'pending')

      }catch( \Exception $e ){
        dd($e);
      }
    }

    public function contractCreate(){
      try{

          // 4685979
         //6881435
               $user = Auth::user();
            $shop = Shop::where('user_id', $user->id)->first();
           $ssContractQuery = $this->subscriptionContractLineItems('gid://shopify/SubscriptionContract/8880283');
          $shopify_customer_id = '4290353627291';
            $ssContractResult =  $this->graphQLRequest($user->id, $ssContractQuery);

            dd($ssContractResult);
            if( !$ssContractResult['errors'] ){
               $subscriptionContract = $ssContractResult['body']->container['data']['subscriptionContract'];
               $sh_lines = (@$subscriptionContract['lines']['edges']) ? $subscriptionContract['lines']['edges'] : [];
               $sh_cpm = ( @$subscriptionContract['customerPaymentMethod']['instrument'] ) ? $subscriptionContract['customerPaymentMethod']['instrument'] : [];
               $sh_deliveryMethod = ( @$subscriptionContract['deliveryMethod'] ) ? $subscriptionContract['deliveryMethod'] : [];
               // dd($subscriptionContract['customerPaymentMethod']);
               $setting = SsSetting::select('subscription_daily_at')->where('shop_id', $shop->id)->first();
               $subscription_daily_at = $setting->subscription_daily_at;
               $timeformat = ( substr($subscription_daily_at, -2));
               $time = substr($subscription_daily_at, 0, 5);

               if( $timeformat == 'PM' && $time == '11:59'){
                 $addtime = 24 . ':00:00';
               }else if( $timeformat == 'PM' && $time == '12:01' ){
                  $addtime = 1 . ':00:00';
               }else if( $timeformat == 'PM' && $time != '12:01' ){
                   $addtime = (12 + substr($subscription_daily_at, 0, 2)) . ':00:00';
               }else{
                    $addtime = substr($subscription_daily_at, 0, 2) . ':00:00';
               }

                // add contract

                $billing_policy = '6month';
                $delivery_policy = '1month';

                $contract = new Ss_contract;
                $contract->shop_id = $shop->id;
                $contract->user_id = $user->id;
                $contract->shopify_contract_id = '5013659';
                $contract->shopify_customer_id = $shopify_customer_id;
                $contract->ss_customer_id = 19;
                $contract->origin_order_id = '2886631719067';
                $contract->status = 'active';

                $default_timezone = date_default_timezone_get();
                date_default_timezone_set($shop->iana_timezone);

                $contract->next_order_date = date("Y-m-d $addtime", strtotime($subscriptionContract['nextBillingDate']));
                $contract->next_processing_date = date("Y-m-d $addtime", strtotime($subscriptionContract['nextBillingDate']));

                date_default_timezone_set($default_timezone);

                $contract->is_prepaid = !strcmp($billing_policy, $delivery_policy);
                $contract->billing_interval = 'MONTH';
                $contract->billing_interval_count = 6;
                $contract->billing_min_cycles = null;
                $contract->billing_max_cycles = null;
                $contract->delivery_interval = 'MONTH';
                $contract->delivery_interval_count = 1;
                $contract->currency_code = 'GBP';
                $contract->lastPaymentStatus = $subscriptionContract['lastPaymentStatus'];
                $contract->order_count = 1;

                $contract->cc_id = str_replace('gid://shopify/CustomerPaymentMethod/', '', $subscriptionContract['customerPaymentMethod']['id']);
                $contract->cc_brand = $sh_cpm['brand'];
                $contract->cc_expiryMonth = $sh_cpm['expiryMonth'];
                $contract->cc_expiryYear = $sh_cpm['expiryYear'];
                $contract->cc_firstDigits = $sh_cpm['firstDigits'];
                $contract->cc_lastDigits = $sh_cpm['lastDigits'];
                $contract->cc_maskedNumber = $sh_cpm['maskedNumber'];
                $contract->cc_name = $sh_cpm['name'];

                $ship_address = (@$sh_deliveryMethod['address']) ? $sh_deliveryMethod['address'] : [];

                if( !empty( $ship_address ) ){
                  $contract->ship_company = (@$ship_address['company']) ? $ship_address['company'] : '';
                  $contract->ship_firstName = (@$ship_address['firstName']) ? $ship_address['firstName'] : '';
                  $contract->ship_lastName = (@$ship_address['lastName']) ? $ship_address['lastName'] : '';
                  $contract->ship_provinceCode = (@$ship_address['provinceCode']) ? $ship_address['provinceCode'] : '';
                  $contract->ship_name = (@$ship_address['name']) ? $ship_address['name'] : '';
                  $contract->ship_address1 = (@$ship_address['address1']) ? $ship_address['address1'] : '';
                  $contract->ship_address2 = (@$ship_address['address2']) ? $ship_address['address2'] : '';
                  $contract->ship_city = (@$ship_address['city']) ? $ship_address['city'] : '';
                  $contract->ship_province = (@$ship_address['province']) ? $ship_address['province'] : '';
                  $contract->ship_zip = (@$ship_address['zip']) ? $ship_address['zip'] : '';
                  $contract->ship_country = (@$ship_address['country']) ? $ship_address['country'] : '';
                  $contract->ship_phone = (@$ship_address['phone']) ? $ship_address['phone'] : '';
                }
                $ship_option = (@$sh_deliveryMethod['shippingOption']) ? $sh_deliveryMethod['shippingOption'] : [];

                if( !empty($ship_option) ){
                  $contract->shipping_code = (@$ship_option['code']) ? $ship_option['code'] : '';
                  $contract->shipping_description = (@$ship_option['description']) ? $ship_option['description'] : '';
                  $contract->shipping_presentmentTitle = (@$ship_option['presentmentTitle']) ? $ship_option['presentmentTitle'] : '';
                  $contract->shipping_title = (@$ship_option['title']) ? $ship_option['title'] : '';
                }
                // dd($contract);
                $contract->save();
                // add contract line items
                if( is_array($sh_lines) && !empty($sh_lines) ){
                    foreach ($sh_lines as $key => $value) {
                        $node = $value['node'];
                        $db_plan = SsPlan::where('shop_id', $shop->id)->where('user_id', $user->id)->where('shopify_plan_id', str_replace('gid://shopify/SellingPlan/', '', $node['sellingPlanId']))->first();
                        $pricingPolicy = $node['pricingPolicy'];

                        if( $db_plan ){
                          $pricing_adjustment_type = $db_plan->pricing_adjustment_type;
                          $pricing_adjustment_value = $db_plan->pricing_adjustment_value;

                          $price = $pricingPolicy['basePrice']['amount'];

                          $discount = 0;
                          if( $pricing_adjustment_value != null && $pricing_adjustment_value != '' ){
                            if( $pricing_adjustment_type == '%' ){
                                $discount = (($price * $pricing_adjustment_value ) / 100);
                            }else{
                                $discount = $pricing_adjustment_value;
                            }
                          }
                          $discounted_price = $price - $discount;
                        }

                        $lineItems = new SsContractLineItem;
                        $lineItems->shopify_contract_id = '5013659';
                        $lineItems->ss_contract_id = $contract->id;
                        $lineItems->user_id = $user->id;
                        $lineItems->shopify_product_id = str_replace('gid://shopify/Product/', '', $node['productId']);
                        $lineItems->shopify_variant_id = str_replace('gid://shopify/ProductVariant/', '', $node['variantId']);
                        $lineItems->price = number_format($pricingPolicy['basePrice']['amount'], 2);
                        $lineItems->price_discounted = number_format( $discounted_price, 2);
                        $lineItems->currency = $node['currentPrice']['currencyCode'];
                        $lineItems->currency_symbol = currencyH($node['currentPrice']['currencyCode']);
                        $lineItems->discount_type = ($pricingPolicy['cycleDiscounts'][0]['adjustmentType'] == 'PERCENTAGE') ? '%' : currencyH($node['currentPrice']['currencyCode']);
                        $lineItems->discount_amount = number_format($discount, 2);
                        $lineItems->final_amount = $discounted_price;
                        $lineItems->quantity = $node['quantity'];
                        $lineItems->selling_plan_id = str_replace('gid://shopify/SellingPlan/', '', $node['sellingPlanId']);
                        $lineItems->selling_plan_name = $node['sellingPlanName'];
                        $lineItems->sku = $node['sku'];
                        $lineItems->taxable = $node['taxable'];
                        $lineItems->title = $node['title'];
                        $lineItems->shopify_variant_image = $node['variantImage']['originalSrc'];
                        $lineItems->shopify_variant_title = $node['variantTitle'];
                        $lineItems->requiresShipping = $node['requiresShipping'];
                        $lineItems->save();

                    }
                }

                // update order
            $db_rates = ExchangeRate::orderBy('created_at', 'desc')->first();
            if( $db_rates ){
                $rate = json_decode($db_rates->conversion_rates);
                $db_order = SsOrder::where('shopify_order_id', '2886631719067')->where('user_id', $user->id)->first();
                if( $db_order ){
                    $tx_fee = ( $user->plan_id == 1 ) ? 0.0075 : 0.0025;
                    $db_order->ss_contract_id = $contract->id;
                    $db_order->conversion_rate = $rate->GBP;
                    $db_order->tx_fee_status = 'pending';
                    $db_order->tx_fee_percentage = $tx_fee;
                    $db_order->tx_fee_amount = number_format(($db_order->order_amount * $tx_fee), 4);
                    $db_order->save();

                    dd($db_order);
                }
            }
            }
            else{
              dd($ssContractResult);
            }

        }catch( \Exception $e ){
            dd($e);
        }
    }

    public function creditCardDetails(){
        $query = '
          {
			  subscriptionContract(id: "gid://shopify/SubscriptionContract/4718747") {
			    customerPaymentMethod {
			      id
			      instrument {
			        ... on CustomerCreditCard {
			          firstDigits
			          brand
			          expiresSoon
			          expiryMonth
			          expiryYear
			          isRevocable
			          lastDigits
			          maskedNumber
			          name
			        }
			      }
			      revokedAt
			    }
			  }
			}
        ';
        dump($query);
        $user = \Auth::user();
        $result =  $this->graphQLRequest($user->id, $query);
        dd($result);
    }

    public function billingAttempt(){
        $query = 'mutation {
                    subscriptionBillingAttemptCreate(subscriptionBillingAttemptInput: {idempotencyKey: "13454553345456"}, subscriptionContractId: "gid://shopify/SubscriptionContract/3670182") {
                      subscriptionBillingAttempt {
                        completedAt
                        createdAt
                        errorMessage
                        id
                        idempotencyKey
                        nextActionUrl
                        subscriptionContract {
                        id
                        originOrder {
                          legacyResourceId
                        }
                      }
                      }
                      userErrors {
                        code
                        field
                        message
                      }
                    }
                  }
                ';
          $user = \Auth::user();
          $result =  $this->graphQLRequest($user->id, $query);
          dd($result);
    }

    public function subscriptionContractLineItems($contractId){
        $query = '
          {
                        subscriptionContract(id: "'. $contractId .'") {
                          lastPaymentStatus
                          nextBillingDate
                          billingAttempts(first: 10) {
                              edges {
                                node {
                                  completedAt
                                  idempotencyKey
                                  createdAt
                                  errorCode
                                  errorMessage
                                  id
                                  nextActionUrl
                                }
                              }
                            }
                          billingPolicy {
                              interval
                              intervalCount
                              maxCycles
                              minCycles
                              anchors {
                                day
                                month
                                type
                              }
                            }
                            deliveryPolicy {
                              interval
                              intervalCount
                              anchors {
                                day
                                month
                                type
                              }
                            }
                            lines (first: 230){
                              edges {
                                node {
                                  id
                                  productId
                                  variantId
                                  currentPrice {
                                    amount
                                    currencyCode
                                  }
                                  pricingPolicy {
                                    basePrice {
                                      amount
                                      currencyCode
                                    }
                                    cycleDiscounts {
                                      adjustmentType
                                      afterCycle
                                      computedPrice {
                                        amount
                                        currencyCode
                                      }
                                    }
                                  }
                                  variantImage {
                                    originalSrc
                                  }
                                  sku
                                  title
                                  quantity
                                  taxable
                                  sellingPlanName
                                  sellingPlanId
                                  requiresShipping
                                  variantTitle
                                  lineDiscountedPrice {
                                    amount
                                    currencyCode
                                  }
                                }
                              }
                            }
                            deliveryMethod {
                            ... on SubscriptionDeliveryMethodShipping {
                              __typename
                              address {
                                address1
                                address2
                                city
                                company
                                country
                                countryCode
                                firstName
                                lastName
                                name
                                phone
                                province
                                provinceCode
                                zip
                              }
                              shippingOption {
                                code
                                description
                                presentmentTitle
                                title
                              }
                            }
                          }
                          customerPaymentMethod {
                              id
                              instrument {
                                ... on CustomerCreditCard {
                                  firstDigits
                                  brand
                                  expiresSoon
                                  expiryMonth
                                  expiryYear
                                  isRevocable
                                  lastDigits
                                  maskedNumber
                                  name
                                }
                              }
                              revokedAt
                            }
                      }
                }
        ';
        // dd($query);
        return $query;
    }

    public function order()
    {
        try {
             // return 'query MyQuery {
             //          sellingPlanGroups(first: 10) {
             //            edges {
             //              cursor
             //              node {
             //                name
             //              }
             //            }
             //          }
             //        }'
             //    ;
            return 'mutation {
                      sellingPlanGroupCreate(input: {name: "Test", merchantCode: "test"}) {
                        sellingPlanGroup {
                          id
                        }
                        userErrors {
                          code
                          field
                          message
                        }
                      }
                    }'
                ;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function request($query)
    {
        $shop = \Auth::user();
        $parameter = [];
        $options = new Options();
        $options->setVersion(env('SHOPIFY_SAPI_VERSION'));
        $api = new BasicShopifyAPI($options);
        $api->setSession(new Session(
            $shop->name, $shop->password));
        return $api->graph($query, $parameter);
    }
}
