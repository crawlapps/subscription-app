<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\PlanRequest;
use App\Models\SsEmail;
use App\Models\SsPlanGroup;
use App\Models\SsSetting;
use App\Traits\ImageTrait;
use App\Traits\ShopifyTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Osiset\ShopifyApp\Storage\Models\Charge;

class SettingController extends Controller
{
    use ShopifyTrait;
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        try{
            $shopID = get_shopID_H();
            $shop = getShopH();

            $setting = SsSetting::where('shop_id', $shopID)->first();
            $email = SsEmail::where('shop_id', $shopID)->where('category', 'failed_payment_to_customer')->first();
            $charge = Charge::where('user_id', $shop->user_id)->where('status', 'ACTIVE')->first();

            $portal = SsSetting::where('shop_id', $shopID)->select(['portal_can_cancel', 'portal_can_skip', 'portal_can_ship_now', 'portal_can_change_qty', 'portal_can_change_nod', 'portal_can_change_freq', 'portal_can_add_product', 'portal_show_content', 'portal_content'])->first();

            $data['setting'] = $setting;
            $data['email'] = $email;
            $data['timezone'] = $shop->iana_timezone;
            $data['portal'] = $portal;
            $data['plan']['plan_id'] = $charge->plan_id;
            $data['plan']['trial_ends'] = strtoupper(date("M d, Y", strtotime($charge->trial_ends_on)));

            $response['data'] = $data;
            $response['themes'] = $this->getThemes();
            return response()->json(['data' => $response], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @return array
     */
    public function getThemes(){
        try{
            $user = Auth::user();
            $parameter['fields'] = 'id,name';
            $sh_themes = $user->api()->rest('GET', 'admin/themes.json', $parameter);

            $theme = [];
            if (!$sh_themes['errors']) {
                $themes = $sh_themes['body']->container['themes'];
                foreach ($themes as $key => $val) {
                    $theme[$key]['id'] = $val['id'];
                    $theme[$key]['name'] = $val['name'];
                }
            }
            return $theme;
        }catch( \Exception $e ){
            logger($e->getMessage());
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePortal(Request  $request){
        try{
            $shopID = get_shopID_H();
            $key = $request->key;
            $customerP = SsSetting::where('shop_id', $shopID)->first();
            $customerP->$key = $request->v;
            $customerP->save();

            return response()->json(['data' => 'Status Changed successfully'], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * store settings
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        try{
            $shopID = get_shopID_H();
            $shop = getShopH();
//            save settings
            $data = $request->data['setting'];
            $setting = SsSetting::where('shop_id', $shopID)->first();

            $setting->dunning_retries = $data['dunning_retries'];
            $setting->dunning_daysbetween = $data['dunning_daysbetween'];
            $setting->dunning_failedaction = $data['dunning_failedaction'];

            if( ($setting->dunning_email_enabled == 0) && ($data['dunning_email_enabled'])){
                $this->event($shop->user_id, 'Onboarding', 'Dunning Email Enabled', '
                                Merchant enabled their dunning email');
            }
            $setting->dunning_email_enabled = $data['dunning_email_enabled'];
            $setting->email_from_name = $data['email_from_name'];
            $setting->email_from_email = $data['email_from_email'];
            $setting->subscription_daily_at = $data['subscription_daily_at'];
            $setting->save();

//            save email
            $data = $request->data['email'];
            $email = SsEmail::where('shop_id', $shopID)->where('category', 'failed_payment_to_customer')->first();
            $email->subject = $data['subject'];
            $email->html_body = $data['html_body'];
            $email->save();

//            save portal
            $data = $request->data['portal'];
            $portal = SsSetting::where('shop_id', $shopID)->first();
            $portal->portal_content = $data['portal_content'];
            $portal->save();

            return response()->json(['data' => 'Saved!'], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * Send test mail
     * @param  PlanRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendMail(PlanRequest $request){
        try{
            $shopID = get_shopID_H();
            $setting = SsSetting::where('shop_id', $shopID)->first();
            $data = $request->data;

            $res = sendMailH($data['subject'], $data['html_body'], $setting->email_from_email, $data['mailto'], $setting->email_from_name);
            return response()->json(['data' => $res], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  PlanRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function emailBody(PlanRequest $request){
        try{
            $shopID = get_shopID_H();
            $data = $request->data;
            $email = SsEmail::where('shop_id', $shopID)->where('category', $data['category'])->first();
            $email->subject = $data['subject'];
            $email->html_body = $data['html_body'];
            $email->save();
            return response()->json(['data' => 'Saved!'], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImage(Request $request){
        try{
            $image = ImageTrait::makeImage($request->image, 'uploads/mail');
            $url = Storage::disk('public')->url('uploads/mail/').$image;
            return response()->json(['data' => $url], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function installTheme(Request $request){
        try{
            $user = Auth::user();
            installThemeH($request['data']['id'], $user->id);
            return response()->json(['data' => 'Theme installed successfully'], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }
}
