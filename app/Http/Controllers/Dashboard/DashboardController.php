<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use App\Models\Ss_contract;
use App\Models\SsContractLineItem;
use App\Models\SsDeletedProduct;
use App\Models\SsOrder;
use App\Traits\ShopifyTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    use ShopifyTrait;
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        try{
            $user = Auth::user();
            $shop = Shop::where('user_id', $user->id)->first();

            $active_subscription = Ss_contract::where('shop_id', $shop->id)->where('user_id', $user->id)->where('status', 'active')->count();
            $pause_subscription = Ss_contract::where('shop_id', $shop->id)->where('user_id', $user->id)->where('status', 'paused')->count();

            $default_timezone = date_default_timezone_get();
            $timezone = $shop->iana_timezone;
            date_default_timezone_set($timezone);
//            dd(date('Y-m-d H:i:s A'));
            $from = date("Y-m-d");
            $to = date('Y-m-d H:i:s');

            $new_subscription = Ss_contract::where('shop_id', $shop->id)->where('user_id', $user->id)->whereBetween('created_at', [$from.' 00:00:00', $to])->count();

            $today_order = SsOrder::where('shop_id', $shop->id)->where('user_id', $user->id)->whereBetween('created_at', [$from.' 00:00:00', $to])->count();

            $today_sales = SsOrder::where('shop_id', $shop->id)->where('user_id', $user->id)->whereBetween('created_at', [$from.' 00:00:00', $to])->sum('order_amount');

            $order = SsOrder::where('shop_id', $shop->id)->where('user_id', $user->id)->first();
            $deletedProduct = SsDeletedProduct::where('user_id', $user->id)->where('shop_id', $shop->id)->where('active', 1)->get()->toarray();
            $data['today_sales'] = ($order) ? $order->currency_symbol . $today_sales . ' ' . $order->order_currency : 0;
            $data['today_order'] = $today_order;
            $data['new_subscription'] = $new_subscription;
            $data['active_subscription'] = $active_subscription;
            $data['paused_subscription'] = $pause_subscription;
            $data['currency'] = $shop->currency;
            $data['currency_symbol'] = $shop->currency_symbol;
            $data['deletedProducts'] = $this->getShopifyVariant($deletedProduct, $user->id);

            date_default_timezone_set($default_timezone);
            return response()->json(['data' => $data], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $deletedProduct
     * @param $userID
     * @return array
     */
    public function getShopifyVariant($deletedProduct, $userID)
    {
        $res = [];
        if( !empty( $deletedProduct ) ){
            foreach ( $deletedProduct as $key=>$val ){
                $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/variants/'.$val['shopify_variant_id'].'.json';
                $parameter['fields'] = 'id,title';
                $result = $this->request('GET', $endPoint, $parameter, $userID);
                $res[$key]['variant_name'] = '';
                if (!$result['errors']) {
                    $sh_variant = $result['body']->container['variant'];
                    $res[$key]['variant_name'] = $sh_variant['title'];
                    $res[$key]['subscriptions_impacted'] = $val['subscriptions_impacted'];
                    $res[$key]['id'] = $val['id'];
                }
            }
        }
        return $res;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function replaceLineItem(Request $request)
    {
        try {
            $user = Auth::user();
            $shop = Shop::where('user_id', $user->id)->first();
            $data = $request->data;
            $deleted_id = $data['deleted_id'];
            $new_resource = $data['resource'][0];

            $deletedProduct = SsDeletedProduct::where('id', $deleted_id)->where('user_id', $user->id)->first();
            $lineItems = SsContractLineItem::where('user_id', $user->id)->where('shopify_product_id', $deletedProduct->shopify_product_id)->where('shopify_variant_id', $deletedProduct->shopify_variant_id)->get();

            if( count($lineItems) > 0 ){
                foreach ( $lineItems as $key=>$val ){
                    $val->shopify_product_id = $new_resource['product_id'];
                    $val->shopify_variant_id = $new_resource['variant_id'];
                    $val->price = $new_resource['price'];

                    if( $val->discount_type == $shop->currency_symbol ){
                        $final_amt = number_format(($new_resource['price'] - $val->discount_amount),2);
                    }elseif ( $val->discount_type == '%' ){
                        $nwprice = $new_resource['price'];
                        $cal_price = ($nwprice * $val->quantity);
                        $final_amt = number_format(($cal_price - (($cal_price * $val->discount_amount) / 100)), 2);
                    }else{
                        $final_amt = number_format($new_resource['price'],2);
                    }
                    $val->final_amount = $final_amt;
                    $val->save();
                }
            }
            $deletedProduct->active = 0;
            $deletedProduct->save();
            return response()->json(['data' => 'Lineitems replace successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }
}
