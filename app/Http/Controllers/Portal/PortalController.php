<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Models\Ss_customer;
use App\User;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Response;
use Osiset\ShopifyApp\Objects\Values\NullableShopDomain;
use function Osiset\ShopifyApp\createHmac;
use function Osiset\ShopifyApp\parseQueryString;
use App\Http\Controllers\Subscriber\SubscriberController;

class PortalController extends Controller
{

    public function index( Request $request){
        try{
            $data['shop'] = $request->shop;
             return Response()->view('portal.index', compact('data'), 200)->withHeaders([
                'Content-Type'=>'application/liquid',
            ]);
//            $subscriberC = new SubscriberController();
            // $domain = $request->shop;
            // $customerId = $request->Customer_Id;
            // $user = User::where('name', $domain)->first();
            // $shop = Shop::where('user_id', $user->id)->first();
            // $subscriber = Ss_customer::with('ActivityLog')->select('id', 'shopify_customer_id', 'first_name', 'last_name',
            //     'active', 'phone', 'email', 'total_orders', 'total_spend', 'total_spend_currency', 'notes',
            //     'avg_order_value')->where('shop_id', $shop['id'])->where('shopify_customer_id', $customerId)->first();

            // $data['customer'] = ( $subscriber ) ?  $subscriberC->orderSubscriber($subscriber, 'portal') : [];
            // $data['shop']['domain'] = $shop['myshopify_domain'];
            // $data['shop']['currency'] = $shop->currency_symbol;
            // // dd($data);
            // return Response()->view('portal.portal', compact('data'), 200)->withHeaders([
            //     'Content-Type'=>'application/liquid',
            // ]);
        }catch( \Exception $e ){
            dd($e);
//            return Response()->view('portal.portal', compact('e'), 422)->withHeaders([
//                'Content-Type'=>'application/liquid',
//            ]);
        }
    }

    public function subscriber( Request $request){
        try{
            // dd($request);
            // //  return Response()->view('portal.portal_test', [], 200)->withHeaders([
            // //     'Content-Type'=>'application/liquid',
            // // ]);
            $subscriberC = new SubscriberController();
            $domain = $request->shop;
            $customerId = $request->CustomerID;
            $user = User::where('name', $domain)->first();
            $shop = Shop::where('user_id', $user->id)->first();
            $subscriber = Ss_customer::with('ActivityLog')->select('id', 'shopify_customer_id', 'first_name', 'last_name',
                'active', 'phone', 'email', 'total_orders', 'total_spend', 'total_spend_currency', 'notes',
                'avg_order_value')->where('shop_id', $shop['id'])->where('shopify_customer_id', $customerId)->first();

            $data['customer'] = ( $subscriber ) ?  $subscriberC->orderSubscriber($subscriber, 'portal', $user) : [];
            $data['shop']['domain'] = $shop['myshopify_domain'];
            $data['shop']['currency'] = $shop->currency_symbol;
            // dd($data);
            return Response()->view('portal.portal', compact('data'), 200)->withHeaders([
                'Content-Type'=>'application/liquid',
            ]);
        }catch( \Exception $e ){
            dd($e);
//            return Response()->view('portal.portal', compact('e'), 422)->withHeaders([
//                'Content-Type'=>'application/liquid',
//            ]);
        }
    }
}
