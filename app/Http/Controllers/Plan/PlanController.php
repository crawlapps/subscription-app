<?php

namespace App\Http\Controllers\Plan;

use App\Http\Controllers\Controller;
use App\Http\Requests\PlanRequest;
use App\Models\Code;
use App\Models\Install;
use App\Models\Shop;
use App\Models\SsEvents;
use App\Models\SsPlan;
use App\Models\SsPlanGroup;
use App\Models\SsPlanGroupVariant;
use App\Traits\ShopifyTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;
use Osiset\ShopifyApp\Storage\Models\Charge;
use Osiset\ShopifyApp\Storage\Models\Plan;
use Illuminate\Support\Facades\DB;

class PlanController extends Controller
{
    use ShopifyTrait;
    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function appPlanIndex(Request $request)
    {
        try {
            $user = \Auth::user();
            $plan_id = $user->plan_id;
            return view('plans.plan', compact('plan_id'));
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function appPlanChange($plan)
    {
        try {
            $user = \Auth::user();
            $new_plan = $plan;
            $db_plan = Plan::where('id', $plan)->first();

            $db_charge = Charge::where('user_id', $user->id)->where('status', 'ACTIVE')->first();

            if( $db_charge ){
                $curr_date = date('Y-m-d H:i:s');
                $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $db_charge->trial_ends_on);
                $from = $curr_date;
                $trial_days = ( $to > $from ) ? $to->diffInDays($from) + 1 : 0;
            }else{
                $trial_days = $plan->trial_days;
            }
            $query = 'mutation{
                 appSubscriptionCreate(
                        name: "'. $db_plan->name .'"
                        returnUrl: "'.env('APP_URL').'/change-plan-db"
                        test: true
                        trialDays: '.$trial_days.'
                        lineItems: [
                            {
                                plan: {
                                    appRecurringPricingDetails: {
                                        price: { amount: '.$db_plan->price.', currencyCode: USD },
                                        interval: '.$db_plan->interval.'
                                    }
                                }
                            }
                        ]
                    ) {
                        appSubscription {
                            id
                        }
                        confirmationUrl
                        userErrors {
                            field
                            message
                        }
                    }
           }';
            $parameters = [];


// Create options for the API
            $options = new Options();
            $options->setVersion('2020-07');

// Create the client and session
            $api = new BasicShopifyAPI($options);
            $api->setSession(new Session(
                $user->name, $user->password));

// Now run your requests...
            $result = $api->graph($query, $parameters);
            $data = $result['body']->container['data'];
            if( !$result['errors'] ){
                return response()->json(['data' => $data], 200);
            }else{
                return response()->json(['data' => $data], 422);
            }
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePlanDB(Request $request){
        try{
            $shop = Auth::user();
            $old_charge = Charge::where('status', 'ACTIVE')->where('user_id', $shop->id)->first();
            if( $old_charge ){
                $old_charge->status = 'CANCELLED';
                $old_charge->cancelled_on = date('Y-m-d H:i:s');
                $old_charge->save();
            }
            $response = $shop->api()->rest("GET",'/admin/api/'.env('SHOPIFY_API_VERSION').'/recurring_application_charges/'.$request->charge_id);
            if( !$response['errors'] ){
                $charge_data = $response['body']->container['recurring_application_charge'];
                $plan = Plan::where('name', $charge_data['name'])->first();
                $charge = new Charge;
                $charge->charge_id = $charge_data['id'];
                $charge->test = $charge_data['test'];
                $charge->status = strtoupper($charge_data['status']);
                $charge->name = $charge_data['name'];
                $charge->interval = $plan->interval;
                $charge->type = 'RECURRING';
                $charge->price = $charge_data['price'];
                $charge->trial_days = $charge_data['trial_days'];
                $charge->billing_on = date("Y-m-d H:i:s", strtotime($charge_data['billing_on']));
                $charge->activated_on = date("Y-m-d H:i:s", strtotime($charge_data['activated_on']));
                $charge->trial_ends_on = date("Y-m-d H:i:s", strtotime($charge_data['trial_ends_on']));
                $charge->created_at = date("Y-m-d H:i:s", strtotime($charge_data['created_at']));
                $charge->updated_at = date("Y-m-d H:i:s", strtotime($charge_data['updated_at']));
                $charge->plan_id = $plan->id;
                $charge->user_id = $shop->id;
                $charge->save();

                $shop->plan_id = $plan->id;
                $shop->save();
            }
            return redirect()->route('home');
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }
    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addEvent(Request $request){
        try{
            $shop = getShopH();
            $res = $this->event($shop->user_id, $request->category, '', $request->description);
            if( $res ){
                return response()->json(['data' => 'Event Added!!'], 200);
            }else{
                return response()->json(['data' => $res], 422);
            }
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function planGroupIndex()
    {
        try {
            $shop = getShopH();
            $plangroup = SsPlanGroup::with('hasManyPlan')->with('hasManyVariants')->where('shop_id',
                $shop->id)->orderBy('position', 'asc')->get()->toArray();
            $data['planG'] = $plangroup;
            $data['shop']['currency'] = $shop->currency_symbol;
            return response()->json(['data' => $data], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function planGroupStore(PlanRequest $request)
    {
        try {
             DB::beginTransaction();
            $data = $request->data;
            $shop = getShopH();
            $planGCount = SsPlanGroup::where('shop_id', $shop->id)->count();
            ( $planGCount > 0 ) ? '' : $this->event($shop->user_id, 'Onboarding', 'First Group Created', 'Merchant created group [' . $data['name'] .']');
            $is_existplan = SsPlanGroup::where('shop_id', $shop->id)->where('id', $data['id'])->first();
            $plangroup = ($is_existplan) ? $is_existplan : new SsPlanGroup;
            $plangroup->shop_id = $shop->id;
            $plangroup->user_id =  $shop->user_id;
            $plangroup->active = 1;
            $plangroup->name = $data['name'];
            $plangroup->merchantCode = ($is_existplan) ? $is_existplan->merchantCode : strtolower(str_replace(' ', '-', $data['description']));
            $plangroup->description = $data['description'];
            $plangroup->position = ($is_existplan) ? $is_existplan->position : SsPlanGroup::where('shop_id',
                $shop->id)->where('active', 1)->count();
            $plangroup->options = implode(',', array_filter($data['options']));
            $plangroup->save();

            // create/edit selling plan in shopify

             $result = ( $plangroup->shopify_plan_group_id ) ? $this->updateSellingPlanGroup($shop->user_id, $plangroup) : '';

             if( $result != '' ){
                if( $result == 'success' ){
                    DB::commit();
                    $msg = 'Saved!';
                    $success = true;
                }else if( !$result ){
                    DB::rollBack();
                    $msg = 'Error';
                    $success = false;
                }else{
                    DB::rollBack();
                    $msg = $result;
                    $success = false;
                }
            }else{
                DB::commit();
                $msg = 'Saved!';
                $success = true;
            }

            return response()->json(['data' => $msg, 'isSuccess' => $success], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  string  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function planGroupEdit($id = '')
    {
        try {
            $shopID = get_shopID_H();
            $plangroup = SsPlanGroup::select('id', 'name', 'description', 'options')->where('shop_id', $shopID)->where('id',
                $id)->first();

            return response()->json(['data' => $plangroup], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function planGroupDestroy($id)
    {
        try {
            $plangroup = SsPlanGroup::where('id', $id)->first();
            $plangroup->delete();
            return response()->json(['data' => 'Deleted!'], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  string  $planGid
     * @param  string  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function planEdit($planGid = '', $id = '')
    {
        try {
            $shop = getShopH();
            $planGroup = SsPlanGroup::select('id', 'name', 'options')->where('shop_id', $shop->id)->where('id', $planGid)->first();
            $plan = SsPlan::where('shop_id', $shop->id)->where('id', $id)->first();
            $data['shop']['currency'] = $shop->currency_symbol;

            $data['plan'] = $plan;
            $data['planGroup'] = $planGroup;
            return response()->json(['data' => $data], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  PlanRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function planStore(PlanRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->data;
            $shop = getShopH();

            $planCount = SsPlan::where('shop_id', $shop->id)->count();
            ( $planCount > 0 ) ? '' : $this->event($shop->user_id, 'Onboarding', 'First Plan Created', 'Merchant created plan [' . $data['name'] .']');

            $is_existplan = SsPlan::where('shop_id', $shop->id)->where('id', $data['id'])->first();
            $plan = ($is_existplan) ? $is_existplan : new SsPlan;
            $plan->shop_id = $shop->id;
            $plan->user_id = $shop->user_id;
            $plan->ss_plan_group_id = $data['ss_plan_group_id'];
            $plan->name = $data['name'];
            $plan->description = $data['description'];
            $plan->options = implode(',', array_filter($data['options']));
            $plan->status = 'active';
            $plan->position = ($is_existplan) ? $is_existplan->position : SsPlan::where('shop_id', $shop->id)->where('ss_plan_group_id', $data['ss_plan_group_id'])->count();
            $plan->billing_interval = $data['billing_interval'];
            $plan->billing_interval_count = $data['billing_interval_count'];
            $plan->delivery_interval = $data['delivery_interval'];
            $plan->delivery_interval_count = $data['delivery_interval_count'];
            $plan->delivery_cutoff = $data['delivery_cutoff'];
            $plan->delivery_pre_cutoff_behaviour = $data['delivery_pre_cutoff_behaviour'];
            $plan->pricing_adjustment_type = $data['pricing_adjustment_type'];
            $plan->pricing_adjustment_value = $data['pricing_adjustment_value'];
            $plan->delivery_intent = $data['delivery_intent'];
            $plan->billing_min_cycles = $data['billing_min_cycles'];
            $plan->billing_max_cycles = $data['billing_max_cycles'];

            if( $data['delivery_intent'] == 'A fixed day each delivery cycle' ){
                $delivery_anchor_type = null;
                if( $data['delivery_interval'] != 'day' ){
                    $delivery_anchor_type = ( $data['delivery_interval'] == 'month' ) ? 'MONTHDAY' : 'WEEKDAY';
                    $delivery_anchor_type = ( $data['delivery_interval'] == 'year' ) ? 'YEARDAY' : $delivery_anchor_type;
                }

                $plan->delivery_anchor_type = $delivery_anchor_type;

                if( $data['delivery_interval'] == 'month' || $data['delivery_interval'] == 'week'  ){
                    $plan->delivery_anchor_day = $data['delivery_anchor_day'];
                }else if( $data['delivery_interval'] == 'year' ){
                    $plan->delivery_anchor_month = $data['delivery_anchor_day'];
                }

                $plan->delivery_cutoff = $data['delivery_cutoff'];
                $plan->delivery_pre_cutoff_behaviour = $data['delivery_pre_cutoff_behaviour'];
            }else{
                $plan->delivery_cutoff = 1;
                $plan->delivery_pre_cutoff_behaviour = 'NEXT';
            }
            $plan->save();

            // create/edit selling plan in shopify
            $result = $this->ShopifySellingPlan($shop->user_id, $plan);
            if( $result == 'success' ){
                DB::commit();
                $msg = 'Saved!';
                $success = true;
            }else if( !$result ){
                DB::rollBack();
                $msg = 'Error';
                $success = false;
            }else{
                DB::rollBack();
                $msg = $result;
                $success = false;
            }
            return response()->json(['data' => $msg, 'isSuccess' => $success], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function planDestroy($id)
    {
        try {
            $plangroup = SsPlan::where('id', $id)->first();
            $plangroup->delete();
            return response()->json(['data' => 'Deleted!'], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignProduct(Request $request)
    {
        try {
             DB::beginTransaction();
            $data = $request->data;
            $shop = getShopH();
            // $exist_variants = SsPlanGroupVariant::where('shop_id',  $shop->id)->where('ss_plan_group_id',
            //     $data['planGroupId'])->get();
            // foreach ($exist_variants as $key => $val) {
            //     $val->delete();
            // }

            $newResource = $data['resource'];
            foreach ($newResource as $key => $val) {
                $variants = new SsPlanGroupVariant;
                $variants->shop_id =  $shop->id;
                $variants->user_id =  $shop->user_id;
                $variants->ss_plan_group_id = $data['planGroupId'];
                $variants->shopify_product_id = $val;
                // $variants->shopify_variant_id = $val['variant_id'];
                $variants->last_sync_date = date('Y-m-d H:i:s');
                $variants->save();
            }

            // assign product in shopify plan group
            $plangroup = SsPlanGroup::where('shop_id', $shop->id)->where('id', $data['planGroupId'])->first();
            // dd('gid://shopify/Product/' . implode(',gid://shopify/Product/', $newResource));
            $result = $this->updateSellingPlanGroupProduct( $shop->user_id, $newResource,  $plangroup->shopify_plan_group_id);

            if( $result == 'success' ){
                DB::commit();
                $msg = 'Saved!';
                $success = true;
            }else if( !$result ){
                DB::rollBack();
                $msg = 'Error';
                $success = false;
            }else{
                DB::rollBack();
                $msg = $result;
                $success = false;
            }
            return response()->json(['data' => $msg, 'isSuccess' => $success], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function position(Request $request)
    {
        try {
            $shop = getShopH();
            $data = $request->data;

            foreach ($data as $key => $val) {
                $plan = $val['plan'];
                foreach ($plan as $pkay => $pval) {
                    $plan = SsPlan::find($pkay);
                    $plan->update(['position' => $pval -1]);
                }
                $plangp = SsPlanGroup::find($key);
                $plangp->update(['position' => $val['index']]);
                $plangp->save();
            }

            // update position in shopify plan/plan group
            $planGroup = SsPlanGroup::where('shop_id', $shop->id)->get();
            if( count($planGroup) > 0 ){
                foreach ($planGroup as $pgkey => $pgvalue) {
                    if( $pgvalue->shopify_plan_group_id ){
                        $pgres = $this->updateSellingPlanGroup($shop->user_id, $pgvalue);

                        $plan = SsPlan::where('shop_id', $shop->id)->where('ss_plan_group_id', $pgvalue->id)->get();
                        logger($pgvalue->id);
                        logger(count($plan));
                        if( count($plan) > 0 ){
                            foreach ($plan as $pkey => $pvalue) {
                               if( $pvalue->shopify_plan_id ){
                                    $pres = $this->updateSellingPlan($shop->user_id, $pgvalue->shopify_plan_group_id, $pvalue);
                               }
                            }
                        }
                    }
                }
            }
            return response()->json(['data' => 'Saved!'], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e], 422);
        }
    }

}
