<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Ss_customer;
use App\Models\Ss_contract;
use App\User;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Response;
use Osiset\ShopifyApp\Objects\Values\NullableShopDomain;
use function Osiset\ShopifyApp\createHmac;
use function Osiset\ShopifyApp\parseQueryString;
use App\Http\Controllers\Subscriber\SubscriberController;

class PortalController extends Controller
{

   public function index( Request $request){
        try{
        	$customer_id = $request->customer;
        	$subscriber = Ss_contract::with('LineItems')->where('shopify_customer_id', $customer_id)->orderBy('created_at', 'desc')->first();
        	 $subscriberC = new SubscriberController();

        	$data['contract'] = [];
        	if( $subscriber ){
        		$user_id = $subscriber->user_id;
        		$shop_id = $subscriber->shop_id;
        		$shop = Shop::find($shop_id);
        		$user = User::find($user_id);

        		$id = $subscriber->id;
        		$next = Ss_contract::select('id')->where('id', '>', $id)->orderBy('id')->first();
            	$previous = Ss_contract::select('id')->where('id', '<', $id)->orderBy('id','desc')->first();

            	$data['contract'] = $subscriber;
            	$data['contract']['lineItems'] = (!empty($subscriber['LineItems'])) ? $subscriberC->getLineItemsData($subscriber, $user) : [];
	 			$data['shop']['domain'] = $shop->myshopify_domain;
	            $data['shop']['currency'] = $shop->currency_symbol;
	            $data['contract']['prev'] = ( $previous ) ? '/subscriber/' . $previous->id . '/edit?page=1' : null;
	            $data['contract']['next'] = ( $next ) ? '/subscriber/' . $next->id . '/edit?page=1' : null;

	            $subscriber->unsetRelation('LineItems');
        	}
        	
            return response()->json(['data' => $data], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }
}
