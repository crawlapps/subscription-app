<?php

namespace App\Http\Controllers\Subscriber;

use App\Exports\SubscribersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubscriberRequest;
use App\Models\Country;
use App\Models\Shop;
use App\Models\Ss_contract;
use App\Models\Ss_customer;
use App\Models\SsActivityLog;
use App\Models\SsComment;
use App\Models\SsContractLineItem;
use App\Models\State;
use App\Traits\ShopifyTrait;
use App\Http\Resources\SubscriptionResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Response;
use function HighlightUtilities\getThemeBackgroundColor;

class SubscriberController extends Controller
{
    use ShopifyTrait;

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $shopID = get_shopID_H();
            $s = $request->s;
            $subscriber = Ss_contract::select('ss_contracts.id','ss_contracts.ss_customer_id', 'ss_contracts.status', 'next_order_date', 'ss_contracts.order_count', 'ss_customers.first_name', 'ss_customers.last_name', 'ss_customers.email', 'ss_customers.phone', 'ss_customers.date_first_order')->join('ss_customers', 'ss_contracts.ss_customer_id', '=', 'ss_customers.id')->where(function ($query) use ($s) {
                $query->where('ss_contracts.status', 'LIKE', '%'.$s.'%')->orWhere('order_count', 'LIKE', '%'.$s.'%')->orWhere('first_name', 'LIKE', '%'.$s.'%')->orWhere('last_name', 'LIKE', '%'.$s.'%')->orWhere('email', 'LIKE', '%'.$s.'%')->orWhere('phone', 'LIKE', '%'.$s.'%');
            })->where('ss_contracts.shop_id', $shopID)->where('ss_customers.shop_id', $shopID)->orderBy('ss_contracts.created_at', 'desc')->paginate(10);

            $res = paginate($subscriber);
            $res['data'] = SubscriptionResource::collection($subscriber);
            return response()->json(['data' => $res], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id, Request $request)
    {
        try {
            $shop = getShopH();
            $user = User::where('id', $shop->user_id)->first();
            $user = Auth::user();
//            $subscriber = Ss_customer::with('ActivityLog')->select('id', 'shopify_customer_id', 'first_name', 'last_name',
//                'active', 'phone', 'email', 'total_orders', 'total_spend', 'total_spend_currency', 'notes',
//                'avg_order_value')->where('shop_id', $shop['id'])->where('id', $id)->first();
            $subscriber = Ss_contract::with('ActivityLog', 'LineItems', 'Customer', 'BillingAttempt')->where('shop_id', $shop['id'])->where('id', $id)->first();

            $next = Ss_contract::select('id')->where('id', '>', $id)->where('shop_id', $shop['id'])->orderBy('id')->first();
            $previous = Ss_contract::select('id')->where('id', '<', $id)->where('shop_id', $shop['id'])->orderBy('id','desc')->first();

            $data['contract'] = $subscriber;
            $data['contract']['lineItems'] = (!empty($subscriber['LineItems'])) ? $this->getLineItemsData($subscriber, $user) : [];
            $data['contract']['activityLog'] = $this->getActivity($subscriber['ActivityLog']);
            $data['contract']['billingAttempt'] = $subscriber->BillingAttempt()->paginate(5, ['*'], 'page', $request->page);
            $data['contract']['fulfillmentOrders'] = ($subscriber->last_billing_order_number) ? $this->getPrepaidFulfillments($user->id, $subscriber->last_billing_order_number) : [];
            $data['otherContracts'] = $this->getOtherContracts($subscriber);
            $subscriber->unsetRelation('ActivityLog');
            $subscriber->unsetRelation('LineItems');
            $subscriber->unsetRelation('BillingAttempt');
//            $data['customer'] = $this->orderSubscriber($subscriber, 'all', $user);
            $data['shop']['domain'] = $shop['myshopify_domain'];
            $data['shop']['currency'] = $shop->currency_symbol;
            $data['contract']['prev'] = ( $previous ) ? '/subscriber/' . $previous->id . '/edit?page=1' : null;
            $data['contract']['next'] = ( $next ) ? '/subscriber/' . $next->id . '/edit?page=1' : null;
            return response()->json(['data' => $data], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $result = 'success';
            $data = $request->data;
            if( @$data['mode'] == 'api' ){
                $domain = $data['shop'];
                $user = User::where('name', $domain)->first();
                $shop = Shop::where('user_id', $user->id)->first();
            }else{
                $shop = getShopH();
            }

            if (@$data['type'] == 'all') {
                $deleted = array_filter($data['deleted']);
                $lineitems = $data['line_items'];
                if (!empty($deleted)) {
                    foreach ($deleted as $key => $val) {
                        SsContractLineItem::find($val)->delete();
                    }
                }
                if (!empty($lineitems)) {
                    foreach ($lineitems as $key => $val) {
                        $lineitem = SsContractLineItem::where('id', $val['id'])->first();
                        $lineitem->quantity = $val['quantity'];
                        $lineitem->discount_amount = $val['discount_amount'];
                        $lineitem->discount_type = $val['discount_type'];
                        $lineitem->final_amount = $val['final_amount'];
                        $lineitem->save();
                    }
                }

                $contract = Ss_contract::find($data['contract_id']);
                $contract->prepaid_renew = $data['prepaid_renew'];
                $contract->save();
//                save customer note
                $subscriber = Ss_customer::find($data['customer_id']);
                $subscriber->update(['notes' => $data['note']]);
                $subscriber->save();
                $msg = 'Subscription saved';
            } elseif ($data['type'] == 'paused' || $data['type'] == 'resumed' || $data['type'] == 'cancelled') {
                $Contract = Ss_contract::find($data['contract_id']);
                $Contract->update(['status' => ($data['type'] == 'resumed') ? 'active' : $data['type']]);
                $Contract->save();
                $msg = 'Subscription '.$data['type'].' successfully';
            } elseif (@$data['type'] == 'reactive') {
                $Contract = Ss_contract::find($data['contract_id']);
                $Contract->update([
                    'status' => 'active', 'next_order_date' => date("Y-m-d H:i:s", strtotime($data['reactiveDate']))
                ]);
                $Contract->save();
                $msg = 'Subscription active successfully';
            }  elseif( @$data['type'] == 'edit_shipping_address1' ){
                $Contract = Ss_contract::find($data['contract_id']);
                $Contract->update([
                    'ship_address1' => $data['ship_add1']
                ]);
                $Contract->save();
                $msg = 'Address updated successfully';
            }  elseif( @$data['type'] == 'skip_next_order' ){
                $Contract = Ss_contract::find($data['contract_id']);
                $next_order_date = $Contract->next_order_date;
                $interval_count = $Contract->delivery_interval_count;
                $interval = $Contract->delivery_interval;
                $new_next_order = date('Y-m-d', strtotime($next_order_date. ' + ' . $interval_count . ' ' . $interval . 's'));
                $Contract->update([
                    'next_order_date' => $new_next_order
                ]);
                $Contract->save();
                $msg = 'Skip order successfully';
            } elseif( @$data['type'] == 'updatePaymentDetailEmail' ){
                $contract = Ss_contract::select('shopify_contract_id')->find($data['contract_id']);
                $result = $this->createCustomerPaymentMethodSendUpdateEmail($shop->user_id, $contract->shopify_contract_id);
            }

             // create/edit selling plan in shopify

//             $result = $this->updateSubscriptionContract($shop->user_id, $Contract);
//
             if( $result != '' ){
                if( $result == 'success' ){
                    DB::commit();
                    $msg = 'Saved!';
                    $success = true;
                }else if( !$result ){
                    DB::rollBack();
                    $msg = 'Error';
                    $success = false;
                }else{
                    DB::rollBack();
                    $msg = $result;
                    $success = false;
                }
            }else{
                DB::commit();
                $msg = 'Saved!';
                $success = true;
            }

            return ( @$data['mode'] == 'api' ) ? true : response()->json(['data' => $msg, 'isSuccess' => $success], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return ( @$data['mode'] == 'api' ) ?  $e : response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $type
     * @param  string  $s
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export($type, $s = '')
    {
        $shopID = get_shopID_H();
        return Excel::download(new SubscribersExport($type, $s, $shopID), 'subscriber.xlsx');
    }

    /**
     * @param  SubscriberRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveComment(SubscriberRequest $request)
    {
        try {
            $shop = getShopH();
            $data = $request->data;
            $user = Auth::user();

            $default_timezone = date_default_timezone_get();
            date_default_timezone_set($shop->iana_timezone);
            $activity = new SsActivityLog();
            $activity->shop_id = $shop->id;
            $activity->user_id = $shop->user_id;
//                $activity->ss_plan_id = $user->plan_id;
            $activity->ss_customer_id = $data['customer_id'];
            $activity->ss_contract_id = $data['id'];
            $activity->user_type = 'user';
            $activity->user_name = $shop->owner;
            $activity->message = $data['msg'];
            $activity->save();

            date_default_timezone_set($default_timezone);
            $msg = 'Activity saved!';

            return response()->json(['data' => $msg], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $subscriber
     * @return false
     */
    public function orderSubscriber($subscriber, $type, $user)
    {
        try {
            if (!empty($subscriber)) {
                if( $type == 'all' ){
                    $contracts = Ss_contract::with('LineItems')->where('ss_customer_id', $subscriber->id)->get()->toArray();
                }else{
                    $contracts = Ss_contract::with('LineItems')->where('ss_customer_id', $subscriber->id)->whereIn('status', ['active', 'paused'])->get()->toArray();
                }
                $c['active'] = [];
                $c['paused'] = [];
                $c['failed'] = [];
                $c['expired'] = [];
                $c['cancelled'] = [];
                if (count($contracts) > 0) {
                    foreach ($contracts as $key => $val) {
                        $val['created_at'] = date("M d, Y", strtotime($val['created_at']));
                        $val['next_order_date'] = date("M d, Y", strtotime($val['next_order_date']));
                        $val['line_items'] = (!empty($val)) ? $this->getLineItemsData($val, $user) : [];
                        if ($val['status'] == 'active') {
                            $c['active'][] = $val;
                        } else {
                            if ($val['status'] == 'paused') {
                                $c['paused'][] = $val;
                            } else {
                                if ($val['status'] == 'failed') {
                                    $c['failed'][] = $val;
                                } else {
                                    if ($val['status'] == 'expired') {
                                        $c['expired'][] = $val;
                                    } else {
                                        if ($val['status'] == 'cancelled') {
                                            $c['cancelled'][] = $val;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $cnt = array_merge($c['active'], $c['paused']);
                $cnt = array_merge($cnt, $c['failed']);
                $cnt = array_merge($cnt, $c['expired']);
                $cnt = array_merge($cnt, $c['cancelled']);
                $subscriber['Contracts'] = $cnt;
                $subscriber['Activities'] = $this->getActivity($subscriber['ActivityLog']);
            }
            return $subscriber;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function getActivity($ActivityLog)
    {
        try {
            $activity = [];
            if (!empty($ActivityLog)) {
                foreach ($ActivityLog as $key => $val) {
                    $curr_date = date('Y-m-d');
                    $activity[$key]['create'] = ( $curr_date > $val['created_at'] ) ? date("F d", strtotime($val['created_at'])) : 'Today';
                    $activity[$key]['time'] = date("H:i A", strtotime($val['created_at']));
                    $activity[$key]['message'] = $val['message'];
                    $activity[$key]['user_type'] = $val['user_type'];
                    $activity[$key]['user_name'] = $val['user_name'];
                }
            }
            return $activity;
        } catch (\Exception $e) {
            dd($e);
        }

    }

    public function saveLineItem(Request $request)
    {
        try {
            $shop = getShopH();
            $data = $request->data;
            $contract_id = $data['contract_id'];
            $variants = $data['resource'];
            foreach ( $variants as $key=>$val ){
                $lineItem = new SsContractLineItem;
                $lineItem->shopify_contract_id = '96325698566';
                $lineItem->ss_contract_id = $contract_id;
                $lineItem->user_id = $shop->user_id;
                $lineItem->shopify_product_id = $val['product_id'];
                $lineItem->shopify_variant_id = $val['variant_id'];
                $lineItem->price = $val['price'];
                $lineItem->currency = $shop->currency;
                $lineItem->currency_symbol = $shop->currency_symbol;
                $lineItem->discount_type = '%';
                $lineItem->discount_amount = 0;
                $lineItem->final_amount = $val['final_amount'];
                $lineItem->quantity = 1;
                $lineItem->save();
            }
            return response()->json(['data' => 'Lineitems saved successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $contract
     * @return array
     */
    public function getLineItemsData($contract, $user)
    {
        try {
            $res = [];
//                $lineItems = SsContractLineItem::where('ss_contract_id', $val['id'])->get()->toArray();
            $lineItems = $contract['LineItems'];
            if (!empty($lineItems)) {
                foreach ($lineItems as $lkey => $lval) {
                    $lval = $lval->toArray();
                    $sh_product = $this->getShopifyProduct($lval['shopify_product_id'], $lval['shopify_variant_id'], $user);
                    $res[$lkey] = array_merge($lval, $sh_product);
                }
            }
            return $res;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * @param $product_id
     * @param $variant_id
     * @return mixed
     */
    public function getShopifyProduct($product_id, $variant_id, $user)
    {
        $shop = $user;
        $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/products/'.$product_id.'.json';
        $parameter['fields'] = 'id,title,image';
        $result = $this->request('GET', $endPoint, $parameter, $shop->id);
        $variant = $this->getShopifyVariant($variant_id, $user);
        if (!$result['errors']) {
            $sh_product = $result['body']->container['product'];
            $res['product_name'] = $sh_product['title'];
            $res['product_image'] = (@$sh_product['image']['src']) ? $sh_product['image']['src'] : noImagePathH();
            $res['variant_name'] = $variant['variant_name'];
            $res['sku'] = $variant['sku'];
        } else {
            $res['product_name'] = '';
            $res['product_image'] = '';
        }
        return $res;
    }

    /**
     * Get shopify variant sku,image
     * @param $variant_id
     * @return mixed
     */
    public function getShopifyVariant($variant_id, $user)
    {
        $shop = $user;
        $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/variants/'.$variant_id.'.json';
        $parameter['fields'] = 'id,title,sku';
        $result = $this->request('GET', $endPoint, $parameter, $shop->id);
        $res['variant_name'] = '';
        $res['sku'] = '';
        if (!$result['errors']) {
            $sh_variant = $result['body']->container['variant'];
            $res['variant_name'] = $sh_variant['title'];
            $res['sku'] = $sh_variant['sku'];
        }
        return $res;
    }

    /**
     * @param $subscriber
     * @return mixed
     */
    public function getOtherContracts($subscriber){
        try {
            $contracts = Ss_contract::select('id', 'status', 'created_at', 'updated_at')->where('ss_customer_id', $subscriber->ss_customer_id)->where('id', '!=', $subscriber->id)->where('shop_id', $subscriber->shop_id)->where('user_id', $subscriber->user_id)->get()->toArray();
            return $contracts;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function getCountry(){
        $data['countries'] = Country::select('name')->get()->toArray();
        $data['states'] = State::select('name')->get()->toArray();
        return response()->json(['data' => $data], 200);
    }


}
