<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class PlanRequest extends FormRequest
{
    public static $rules = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Self::$rules;
        $data = $this::all();
        $data = $data['data'];
        switch (Route::currentRouteName()) {
            case 'plan-group':
            {
                $rules['data.name'] = 'required';
                $rules['data.options.0'] = 'required';
                break;
            }
            case 'plan.store':
            {
                $rules['data.name'] = 'required';
                $rules['data.description'] = 'required';
                $rules['data.billing_interval_count'] = 'integer|min:1';
                $rules['data.delivery_interval_count'] = 'integer|min:1';
                $rules['data.delivery_cutoff'] = 'integer|min:1|max:31';
                $rules['data.billing_min_cycles'] = 'nullable|integer|min:1|';
                $rules['data.billing_max_cycles'] = 'nullable|integer|min:1|';

                foreach( $data['options'] as $key=>$val ){
                    $rules['data.options.' . $key] = 'required';
                }
                break;
            }
            case 'mail':
            {
                $rules['data.subject'] = 'required';
                $rules['data.html_body'] = 'required';
                $rules['data.mailto'] = 'required|email';
                break;
            }
            case 'email-body':
            {
                $rules['data.html_body'] = 'required';
                break;
            }
            default:
                break;
        }
        return $rules;
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        $rules = [];
        $data = $this::all();
        $data = $data['data'];

        $rules['data.name.*'] = 'required';
        $rules['data.options.0.*'] = 'required';
        $rules['data.options.1.*'] = 'required';
        $rules['data.description.*'] = 'required';
        $rules['data.billing_interval_count.*'] = 'must be at least 1';
        $rules['data.delivery_interval_count.*'] = 'must be at least 1';
        $rules['data.delivery_interval_count.*'] = 'must be at least 1';
        $rules['data.delivery_cutoff.min'] = 'must be at least 1';
        $rules['data.delivery_cutoff.max'] = 'must be maximum 31';
        $rules['data.delivery_anchors.*'] = 'must be at least 1';
        $rules['data.billing_min_cycles.min'] = 'must be at least 1';
        $rules['data.billing_max_cycles.min'] = 'must be at least 1';
        $rules['data.html_body.*'] = 'required';
        $rules['data.subject.*'] = 'required';
        $rules['data.mailto.required'] = 'required';
        $rules['data.mailto.email'] = 'Enter valid email address';
        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }
}
