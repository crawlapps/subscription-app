<?php

use App\Models\Shop;
use App\Models\ExchangeRate;
use App\User;
use App\Constants\Api;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Intl\Currencies;

if (!function_exists('get_shopID_H')) {

    /**
     * @return mixed
     *
     */
    function get_shopID_H()
    {
        $user = Auth::user();
        $shop = Shop::where('user_id', $user->id)->first();
        return $shop['id'];
    }
}

if (!function_exists('getShopH')) {

    /**
     * @return mixed
     * @return mixed
     */
    function getShopH()
    {
        $user = Auth::user();
        $shop = Shop::where('user_id', $user->id)->first();
        return $shop;
    }
}

if (!function_exists('sendMailH')) {

    /**
     * @return mixed
     * @return mixed
     */
    function sendMailH($subject, $html, $from, $to, $fromname)
    {
        try{
            $data = array('data' => $html);
            Mail::send('mail.mail', $data, function ($message) use ($subject, $html, $from, $to, $fromname) {
                $message->from($from, $fromname);
                $message->to($to);
                $message->subject($subject);
            });
            return 'Mail Send Successfully!';
        }catch( \Exception $e ){
            return $e->getMessage();
        }

    }
}
if (!function_exists('currencyH')) {

    /**
     * @return mixed
     * @return mixed
     */
    function currencyH($c)
    {
        return Currencies::getSymbol($c);
    }
}
if (!function_exists('noImagePathH')) {
    /**
     * @return mixed
     */
    function noImagePathH()
    {
        return asset('images/static/no-image-box.png');
    }
}

if (!function_exists('calculateCurrency')) {
    /**
     * currency converter
     */
     function calculateCurrency($fromCurrency, $toCurrency, $amount)
    {
        try{
            $db_rates = ExchangeRate::latest()->first();
            $rates = json_decode($db_rates->conversion_rates);

            $calculated = round((( $amount * $rates->$toCurrency ) / $rates->$fromCurrency), 4);
            return $calculated;
        }catch(\Exception $e){
            logger('=========== calculateCurrency ===========');
            logger($e);
        }

    }

}

if (!function_exists('installThemeH')) {
    /**
     * @return mixed
     */
    function installThemeH($theme_id, $user_id)
    {
        addSnippetH($theme_id, $user_id);
        addCSSAsset($theme_id, $user_id);
    }
}

if (!function_exists('addSnippetH')) {
    /**
     * @param $theme_id
     */
    function addSnippetH($theme_id, $user_id){
        try {
            \Log::info('-----------------------START :: addSnippet -----------------------');
            $user = User::where('id', $user_id)->first();

            $type = 'add';
            if ($type == 'add') {
                $value = <<<EOF
{% if customer %}
  <script type="text/javascript">
    sessionStorage.setItem("X-shopify-customer-ID",{{ customer.id }});
  </script>
{% endif %}
EOF;
            }
            $parameter['asset']['key'] = 'snippets/simplee.liquid';
            $parameter['asset']['value'] = $value;
            $asset = $user->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);

            updateThemeLiquidH('simplee', $theme_id);
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: addSnippet -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

if (!function_exists('updateThemeLiquidH')) {
    /**
     * @param $snippet_name
     * @param $theme_id
     */
    function updateThemeLiquidH($snippet_name, $theme_id){
        try {
            \Log::info('-----------------------START :: updateThemeLiquidH -----------------------');
            $shop = \Auth::user();

            \Log::info('-----------------------START:: updateThemeLiquid-----------------------');
            $asset = $shop->api()->rest('GET', 'admin/themes/'.$theme_id.'/assets.json',
                ["asset[key]" => 'layout/theme.liquid']);

            if (@$asset['body']['container']['asset']) {
                $asset = $asset['body']->container['asset']['value'];
                // add after <body>
                if (!strpos($asset, "{% include '$snippet_name' %}")) {
                    $asset = str_replace('</body>', "{% include '$snippet_name' %}</body>", $asset);
                }

                $parameter['asset']['key'] = 'layout/theme.liquid';
                $parameter['asset']['value'] = $asset;
                $result = $shop->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);
            }
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: updateThemeLiquidH -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

if (!function_exists('addCSSAsset')) {
    /**
     * @param $theme_id
     */
    function addCSSAsset($theme_id, $user_id){
        try {
            \Log::info('-----------------------START :: addCSSAsset -----------------------');
            $user = User::where('id', $user_id)->first();
            $cssCode = getCSSCode();

            $parameter['asset']['key'] = 'assets/simplee.css';
            $parameter['asset']['value'] = $cssCode;
            $asset = $user->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);

            \Log::info(json_encode($asset));
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: addCSSAsset -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

if (!function_exists('getCSSCode')) {
    /**
     * @return string
     */
    function getCSSCode()
    {
        // Asset Simplee CSS
        $css_code = 'simplee test css';
        return $css_code;
    }
}

if (!function_exists('paginate')) {
    /**
     * @return string
     */
    function paginate($entity)
    {
        $res = [
            'from' => $entity->firstItem(),
            'to' => $entity->lastItem(),
            'total' => $entity->total(),
            'count' => $entity->count(),
            'per_page' => $entity->perPage(),
            'current_page' => $entity->currentPage(),
            'total_pages' => $entity->lastPage(),
            'prev_page_url' => $entity->previousPageUrl(),
            'next_page_url' => $entity->nextPageUrl(),
        ];
        return $res;
    }
}
