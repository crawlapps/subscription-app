<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SsPlan extends Model
{
    protected $fillable = ['position'];
}
