<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SsPlanGroup extends Model
{
    use SoftDeletes;
    protected $fillable = ['position'];
    public function hasManyPlan(){
        return $this->hasMany(SsPlan::class, 'ss_plan_group_id', 'id' )->orderBy('position');
    }
    public function hasManyVariants(){
        return $this->hasMany(SsPlanGroupVariant::class, 'ss_plan_group_id', 'id' );
    }
}
