<?php

namespace App\Exports;

use App\Models\Ss_customer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SubscribersExport implements FromCollection , WithHeadings
{
    private $shopID;
    private $type;
    private $s;
    public function __construct($type, $s, $id)
    {
        \Log::info('Export maatwebsite');
        $this->shopID = $id;
        $this->type = $type;
        $this->s = $s;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $entities = $this->getData();
        $entity = $this->createFile($entities);
        return collect($entity);
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $s = $this->s;
        $t = $this->type;

        if( $t == 'all' ){
            $subscriber = Ss_customer::where('shop_id', $this->shopID)
                ->where(function ($query) use ($s) {
                    $query->where('first_name', 'LIKE', '%'.$s.'%')->orWhere('last_name', 'LIKE', '%'.$s.'%')->orWhere('email', 'LIKE', '%'.$s.'%')->orWhere('phone', 'LIKE', '%'.$s.'%');
                })->orderBy('created_at', 'desc')->get();
        } elseif( $t == 'active' || $t == 'inactive' ){
            $a = ( $t == 'active' ) ? 1 : 0;
            $subscriber = Ss_customer::where('shop_id', $this->shopID)->where('active', $a)
                ->where(function ($query) use ($s) {
                    $query->where('first_name', 'LIKE', '%'.$s.'%')->orWhere('last_name', 'LIKE', '%'.$s.'%')->orWhere('email', 'LIKE', '%'.$s.'%')->orWhere('phone', 'LIKE', '%'.$s.'%');
                })->orderBy('created_at', 'desc')->get();
        }else{
            $subscriber = collect([]);
        }
        return $subscriber;
    }

    /**
     * @param $entities
     * @return mixed
     */
    public function createFile($entities){
        \Log::info('----------create File--------------');
        if( $entities ){
            $entity = $entities->map(function ($name){
                $data = [
                    'status' => ( $name->active ) ? 'Active' : 'Inactive',
                    'name' =>  $name->first_name . ' ' . $name->last_name,
                    'email' => $name->email,
                    'phone' => $name->phone,
                    'total_orders' => ($name->total_orders  > 1 ) ? $name->total_orders. ' orders' : $name->total_orders. ' order',
                    'total_spent' => $name->total_spend_currency . ' '. $name->total_spend,

                ];
                return $data;
            });
        }
        return $entity;
    }

    public function headings(): array
    {
        return [
            'status',
            'name',
            'email',
            'phone',
            'total_orders',
            'total_spent',
        ];
    }
}
