<?php

namespace App\Observers;

use App\Models\Install;
use App\Models\Shop;
use App\Models\SsSetting;
use App\User;
use Osiset\ShopifyApp\Storage\Models\Charge;
use Osiset\ShopifyApp\Storage\Models\Plan;

class UserObserver
{
    public function saving(User $user)
    {
        $old_plan = $user->getOriginal('plan_id');
        $new_plan = $user->plan_id;
//        logger('Old plan :: ' .$old_plan);
//        logger('new plan :: ' .$new_plan);
        if( $old_plan != $new_plan ){
            logger('user observer');
            $plan = Plan::find($user->plan_id);
            $shop = Shop::where('user_id', $user->id)->first();
            $setting = SsSetting::where('shop_id', $shop->id)->first();
            $setting->billing_plan_id = $user->plan_id;
            $setting->transaction_fee = $plan->transaction_fee;
            $setting->save();
//
//            $charge = Charge::where('status', 'ACTIVE')->where('user_id', $shop->user_id)->first();
//            $install->billing_plan_id = $charge['plan_id'];
//            $install->recurring_charge_id = $charge['charge_id'];
//            $install->active = true;
//            $install->trial_expires = $charge['trial_ends_on'];
//            $install->billing_plan_current_cap = $charge['capped_amount'];
////            $install->transaction_fee_rate = $charge['capped_amount'];
//            $install->shopify_charge_status = $charge['status'];
//            $install->deleted = false;
//            $install->save();
        }
    }
}
