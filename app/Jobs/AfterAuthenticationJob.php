<?php

namespace App\Jobs;

use App\Traits\ShopifyTrait;
use App\Models\Shop;
use App\Models\SsEmail;
use App\Models\SsSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Osiset\ShopifyApp\Storage\Models\Plan;

class AfterAuthenticationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyTrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger('============== START:: AfterAuthenticationJob ===========');

            $user = Auth::user();
            logger(json_encode($user));
            $user->active = 1;
            $user->save();
            $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/shop.json';
            $result = $user->api()->rest('GET', $endPoint);
            if( !$result['errors'] ){
                $sh_shop = $result['body']->container['shop'];

                $is_exist_shop = Shop::where('user_id', $user->id)->first();
                $db_shop = ( $is_exist_shop ) ? $is_exist_shop : new Shop;
                $db_shop->user_id = $user['id'];
                $db_shop->shopify_store_id = $sh_shop['id'];
                $db_shop->active = true;
                $db_shop->ts_last_deactivation = date('Y-m-d H:i:s');
                $db_shop->test_store = true;
                $db_shop->name = $sh_shop['name'];
                $db_shop->email = $sh_shop['email'];
                $db_shop->myshopify_domain = $sh_shop['myshopify_domain'];
                $db_shop->domain = $sh_shop['domain'];
                $db_shop->owner = $sh_shop['shop_owner'];
                $db_shop->owner = $sh_shop['shop_owner'];
                $db_shop->shopify_plan = $sh_shop['plan_name'];
                $db_shop->timezone = $sh_shop['timezone'];
                $db_shop->address1 = $sh_shop['address1'];
                $db_shop->address2 = $sh_shop['address2'];
                $db_shop->checkout_api_supported = $sh_shop['checkout_api_supported'];
                $db_shop->city = $sh_shop['city'];
                $db_shop->country = $sh_shop['country'];
                $db_shop->country_code = $sh_shop['country_code'];
                $db_shop->country_name = $sh_shop['country_name'];
                $db_shop->country_taxes = $sh_shop['county_taxes'];
                $db_shop->ss_created_at = ($is_exist_shop) ? $is_exist_shop['ss_created_at'] : date('Y-m-d H:i:s');
                $db_shop->customer_email = $sh_shop['customer_email'];
                $db_shop->currency = $sh_shop['currency'];
                $db_shop->currency_symbol = currencyH($sh_shop['currency']);
                $db_shop->enabled_presentment_currencies = json_encode($sh_shop['enabled_presentment_currencies']);
                $db_shop->eligible_for_payments = $sh_shop['eligible_for_payments'];
                $db_shop->has_discounts = $sh_shop['has_discounts'];
                $db_shop->has_gift_cards = $sh_shop['has_gift_cards'];
                $db_shop->has_storefront = $sh_shop['has_storefront'];
                $db_shop->iana_timezone = $sh_shop['iana_timezone'];
                $db_shop->latitude = $sh_shop['latitude'];
                $db_shop->longitude = $sh_shop['longitude'];
                $db_shop->money_format = $sh_shop['money_format'];
                $db_shop->money_in_emails_format = $sh_shop['money_in_emails_format'];
                $db_shop->money_with_currency_format = $sh_shop['money_with_currency_format'];
                $db_shop->money_with_currency_in_emails_format = $sh_shop['money_with_currency_in_emails_format'];
                $db_shop->multi_location_enabled = $sh_shop['multi_location_enabled'];
                $db_shop->password_enabled = $sh_shop['password_enabled'];
                $db_shop->phone = $sh_shop['phone'];
                $db_shop->pre_launch_enabled = $sh_shop['pre_launch_enabled'];
                $db_shop->primary_locale = $sh_shop['primary_locale'];
                $db_shop->province = $sh_shop['province'];
                $db_shop->province_code = $sh_shop['province_code'];
                $db_shop->requires_extra_payments_agreement = $sh_shop['requires_extra_payments_agreement'];
                $db_shop->setup_required = $sh_shop['setup_required'];
                $db_shop->taxes_included = $sh_shop['taxes_included'];
                $db_shop->tax_shipping = $sh_shop['tax_shipping'];
                $db_shop->tbl_updated_at = date('Y-m-d H:i:s');
                $db_shop->weight_unit = $sh_shop['weight_unit'];
                $db_shop->zip = $sh_shop['zip'];
                $db_shop->save();

                $setting = SsSetting::where('shop_id', $db_shop->id)->first();
                if( !$setting ){

                    $setting = new SsSetting;
                    $setting->shop_id = $db_shop->id;
                    $setting->dunning_retries = 7;
                    $setting->dunning_daysbetween = 0;
                    $setting->email_from_name = $sh_shop['shop_owner'];
                    $setting->email_from_email = $sh_shop['email'];
                    $setting->dunning_email_enabled = 0;
                    $setting->dunning_failedaction = 'cancel';
                    $setting->subscription_daily_at = '12:01 AM';
                    $setting->save();
                }

                $email = SsEmail::where('shop_id', $db_shop->id)->where('category', 'failed_payment_to_customer')->first();
                if( !$email ){
                    $email = new SsEmail;
                    $email->shop_id = $db_shop->id;
                    $email->category = 'failed_payment_to_customer';
                    $email->description = '';
                    $email->active = 1;
                    $email->subject = 'Your credit card failed!!';
                    $email->plain_text = '';
                    $email->html_body = '<p>Your Credit card has failed</p>';
                    $email->save();
                }
            }

            // create webhooks
//            $webhooks = [
//                'subscription_billing_attempts/failure' => 'subscription-billing-attempts-failure',
//                'subscription_billing_attempts/success' => 'subscription-billing-attempts-success',
//                'subscription_contracts/create' => 'subscription-contracts-create',
//                'subscription_contracts/update' => 'subscription-contracts-update',
//                'customer_payment_methods/create' => 'customer-payment-methods-create',
//                'customer_payment_methods/revoke' => 'customer-payment-methods-revoke',
//                'customer_payment_methods/update' => 'customer-payment-methods-update',
//                ];
//            $this->createWebhook($webhooks, $user->id);

            logger('============== END:: AfterAuthenticationJob ===========');
        }catch( \Exception $e ){
            logger('============== ERROR:: AfterAuthenticationJob ===========');
            logger(json_encode($e));
        }
    }
}
