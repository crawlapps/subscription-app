<?php namespace App\Jobs;

use App\Models\Shop;
use App\Models\Ss_customer;
use App\Traits\ShopifyTrait;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use stdClass;

class CustomersDeleteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyTrait;
    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain
     * @param stdClass $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger('========= START:: customer delete webhook =========');
        $domain = $this->shopDomain->toNative();
        $user = User::where('name', $domain)->first();
        $shop = Shop::where('user_id', $user->id)->first();
        $data = $this->data;

        $this->webhook('customers/delete', $user->id, json_encode($this->data));

        $customer = Ss_customer::where('shop_id', $shop->id)->where('shopify_customer_id', $data->id)->first();
        logger(json_encode($customer));
        if( $customer ){
            $customer->delete();
        }
    }
}
