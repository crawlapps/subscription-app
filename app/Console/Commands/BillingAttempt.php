<?php

namespace App\Console\Commands;

use App\Models\Shop;
use App\Models\Ss_contract;
use App\Models\SsBillingAttempt;
use App\Traits\ShopifyTrait;
use App\User;
use Illuminate\Console\Command;

class BillingAttempt extends Command
{
    use ShopifyTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'billing:attempt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to check for contracts that require a billing attempt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
            logger( '============= START:: Billing Attempt ==========' );
            $users = User::where('active', 1)->get();
            foreach ( $users as $ukey=>$uval ){
                $shop = Shop::where('user_id', $uval->id)->latest()->first();
                $currentTime = date('Y-m-d H:i:s');
                 $db_contract = Ss_contract::where('user_id', $uval->id)->where('status', 'active')->whereDate('next_processing_date', '<=', $currentTime)
                              ->where(function ($query) {
                                   $query->where('status_billing', '!=', 'pending')->orWhere('status_billing', NULL);
                              })->get();
                if( count($db_contract) > 0 ){
                    foreach ( $db_contract as $ckey=>$cval ){
                        $result = $this->billingAttempt($cval->shopify_contract_id, $uval->id);
                        if( !$result['errors'] ){
                            $sh_billingAttempt = $result['body']->container['data']['subscriptionBillingAttemptCreate'];
                            if( empty($sh_billingAttempt['userErrors'] )){
                                $subscriptionBillingAttempt = $sh_billingAttempt['subscriptionBillingAttempt'];

                                $cval->status_billing = 'pending';
                                $cval->save();

                                $billingAttempt = new SsBillingAttempt;
                                $billingAttempt->shop_id = $shop->id;
                                $billingAttempt->shopify_id = str_replace('gid://shopify/SubscriptionBillingAttempt/', '', $subscriptionBillingAttempt['id']);
                                $billingAttempt->ss_contract_id = $cval->id;
                                $billingAttempt->status = 'sent';
                                $billingAttempt->completedAt = date('Y-m-d H:i:s', strtotime($subscriptionBillingAttempt['completedAt']));
                                $billingAttempt->errorMessage = $subscriptionBillingAttempt['errorMessage'];
                                $billingAttempt->idempotencyKey = $subscriptionBillingAttempt['idempotencyKey'];
                                $billingAttempt->nextActionUrl = $subscriptionBillingAttempt['nextActionUrl'];
                                $billingAttempt->shopify_contract_id = (@$subscriptionBillingAttempt['subscriptionContract']['id']) ? str_replace('gid://shopify/SubscriptionContract/', '', $subscriptionBillingAttempt['subscriptionContract']['id']): '';
                                $billingAttempt->shopify_order_id = (@$subscriptionBillingAttempt['subscriptionContract']['originOrder']['legacyResourceId']) ? $subscriptionBillingAttempt['subscriptionContract']['originOrder']['legacyResourceId'] : '';
                                $billingAttempt->save();
                            }else{
                                logger('=============== Shopify billing attempt user ERROR ================');
                                logger(json_encode($sh_billingAttempt['userErrors']));
                            }
                        }else{
                            logger('=============== Shopify billing attempt graphQL ERROR ================');
                            logger(json_encode($result));
                        }
                        return false;
                    }
                }
            }
            logger( '============= END:: Billing Attempt ==========' );
        }catch ( \Exception $e ){
            logger( '============= ERROR:: Billing Attempt ==========' );
            logger(json_encode($e));
        }
        return 0;
    }
    public function billingAttempt($contractId, $user_id){
        $idempotencyKey = $user_id.$contractId.date('YmdHis');
        $query = 'mutation {
                    subscriptionBillingAttemptCreate(subscriptionBillingAttemptInput: {idempotencyKey: "'.$idempotencyKey.'"}, subscriptionContractId: "gid://shopify/SubscriptionContract/'.$contractId.'") {
                      subscriptionBillingAttempt {
                        completedAt
                        createdAt
                        errorMessage
                        id
                        idempotencyKey
                        nextActionUrl
                        subscriptionContract {
                            id
                            originOrder {
                                legacyResourceId
                            }
                        }
                      }
                      userErrors {
                        code
                        field
                        message
                      }
                    }
                  }
                ';
        $result =  $this->graphQLRequest($user_id, $query);
        return $result;
    }
}
