<?php

namespace App\Console\Commands;

use App\Models\SsOrder;
use App\Traits\ShopifyTrait;
use App\User;
use Illuminate\Console\Command;
use Osiset\ShopifyApp\Storage\Models\Charge;
use SebastianBergmann\GlobalState\TestFixture\SnapshotTrait;

class UsageCharge extends Command
{
    use ShopifyTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'usage:charge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create usage charge';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        logger('=============== START:: usage charge cron ==============');
        try{
            $user = User::where('name', 'simplee-test-4.myshopify.com')->first();
//            $this->createUsageCharge($user->id);
            $this->createUsageCharge(1);
//            $orders = SsOrder::where('tx_fee_status', 'pending')->get();
//            if( count($orders) > 0 ){
//                foreach ( $orders as $key=>$val ){
//                    $this->createUsageCharge($val->user_id, $val->id);
//                }
//            }
        } catch (\Exception $e) {
            logger('=============== ERROR:: usage charge cron ==============');
            logger($e);
        }
        return 0;
    }

    /**
     * @param $user_id
     */
    public function createUsageCharge($user_id){
        try{
            logger('=============== START:: createUsageCharge ==============');
            $charge = Charge::where('user_id', $user_id)->where('status', 'ACTIVE')->first();
            $user = User::where('id', $user_id)->first();
            $endPoint = '/admin/api/'. env('SHOPIFY_API_VERSION') .'/recurring_application_charges/'. $charge->charge_id .'/usage_charges.json';
            $charge = [
                    "usage_charge"=> [
                    "description"=> "usage charge",
                    "price"=> 0.5
                 ]
            ];
            $result = $user->api()->rest('POST', $endPoint, $charge);
            logger(json_encode($result));
//            $order = Order::where('id', $order_id)->first();
//            if( $result['errors'] ){
//                $order->tx_fee_status = $result['body']['usage_charge'];
//            }else{
//                $order->tx_fee_status = 'paid';
//            }
//            $order->save();
        } catch (\Exception $e) {
            logger('=============== ERROR:: createUsageCharge ==============');
            logger($e);
        }
    }
}
