<?php

namespace App\Traits;

use App\Exceptions\ApiOperationFailedException;
use App\Models\App;
use App\Models\ExchangeRate;
use App\Models\Shop;
use App\Models\SsOrder;
use App\Models\SsPlan;
use App\Models\SsPlanGroup;
use App\Models\SsEvents;
use App\Models\SsSetting;
use App\Models\SsWebhook;
use App\User;
use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Osiset\ShopifyApp\Storage\Models\Plan;
use Symfony\Component\HttpFoundation\Response;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;

/**
 * Trait ShopifyTrait.
 */
trait ShopifyTrait
{
    /**
     * @param $method
     * @param $endPoint
     * @param $parameter
     * @param $userID
     * @return false
     */
    public function request($method, $endPoint, $parameter, $userID){
        try{
            $shop = User::where('id', $userID)->first();
            return $shop->api()->rest($method, $endPoint, $parameter);
        }catch( \Exception $e ){
            return false;
        }
    }

    /**
     * Add webhook while any of the webhook calling
     * @param $topic
     * @param $user_id
     * @param $data
     * @return false
     */
    public function webhook($topic, $user_id, $data){
        try{
            $app = App::where('app_url', env('APP_URL'))->first();
            $shop = Shop::where('user_id', $user_id)->first();

            $ss_webhook = new SsWebhook();
            $ss_webhook->topic = $topic;
            $ss_webhook->user_id = $user_id;
            $ss_webhook->shop_id = $shop->id;
            $ss_webhook->api_version = $app['api_version'];
            $ss_webhook->body = $data;
            $ss_webhook->status = 'new';
            $ss_webhook->save();
            return $ss_webhook->id;
        }catch( \Exception $e ){
            return false;
        }
    }

    public function createWebhook($webhooks, $user_id){
        try{
            foreach ($webhooks as $key => $value) {
                $endPoint = 'admin/api/' . env('SHOPIFY_SAPI_VERSION') . '/webhooks/count.json';
                $parameter['topic'] = $key;
                $result = $this->request('GET', $endPoint, $parameter, $user_id);

                if( !$result['errors'] ){
                    $count = $result['body']->container['count'];
                    if( $count == 0 ){
                        $parameter = [
                            'webhook' => [
                                'topic' => $key,
                                'address' => env('APP_URL') . '/webhook/' . $value,
                                'format' => 'json'
                            ]
                        ];

                        logger(json_encode($parameter));
                        $endPoint = 'admin/api/' . env('SHOPIFY_SAPI_VERSION') . '/webhooks.json';
                        $result = $this->request('POST', $endPoint, $parameter, $user_id);

                        logger( json_encode($result) );
                    }
                }
            }
         }catch( \Exception $e ){
            logger('============= ERROR:: createWebhook =============');
            logger( json_encode($e) );
            return false;
        }
    }

    /**
     * @param $user_id
     * @param $category
     * @param $subCategory
     * @param $des
     * @return bool|string
     */
    public function event($user_id, $category, $subCategory, $des){
        try{
            $shop = Shop::where('user_id', $user_id)->first();
            $events = new SsEvents;
            $events->shop_id = $shop->id;
            $events->user_id = $user_id;
            $events->myshopify_domain = $shop->myshopify_domain;
            $events->category = $category;
            $events->subcategory = $subCategory;
            $events->description = $des;
            $events->save();
            return true;
        }catch( \Exception $e ){
            return $e->getMessage();
        }
    }

    /**
     * @param $user_id
     * @param $query
     * @param  array  $parameter
     * @return array|\GuzzleHttp\Promise\Promise
     */
    public function graphQLRequest($user_id, $query, $parameter = []){
        try{
            $user = User::find($user_id);
            $options = new Options();
            $options->setVersion(env('SHOPIFY_SAPI_VERSION'));
            $api = new BasicShopifyAPI($options);
            $api->setSession(new Session(
                $user->name, $user->password));
            return $api->graph($query, $parameter);
        }catch( \Exception $e )
        {
            logger('============ ERROR:: graphQLRequest ===========');
            logger(json_encode($e));
        }
    }

    /**
     * Make GraphQL api request
     * @param $user_id
     * @param $planData
     */
    public function ShopifySellingPlan($user_id, $planData){
        try{
            logger($planData);
            $plan_gp_id = $planData->ss_plan_group_id;
            $action = ( SsPlan::where('ss_plan_group_id', $plan_gp_id)->count() <= 1 ) && !$planData->shopify_plan_id ? 'create' : 'update';
            $query = ( $action == 'create' ) ? $this->createSellingPlanQuery($planData) : $this->updateSellingPlanQuery($planData);
            $result = $this->graphQLRequest($user_id, $query);
            logger($query);
            logger(json_encode($result));
            if( !$result['errors'] ){
                if( $action == 'create'){
                  $sh_sellingplan = $result['body']->container['data']['sellingPlanGroupCreate'];
                }else{
                  $sh_sellingplan = $result['body']->container['data']['sellingPlanGroupUpdate'];
                }

                if( empty($sh_sellingplan['userErrors'] )){
                    $sh_sellingPlanGroup = $sh_sellingplan['sellingPlanGroup'];
                    $planGroupData = SsPlanGroup::where('id', $plan_gp_id)->first();

                    if( !$planGroupData->shopify_plan_group_id ){
                      $planGroupData->shopify_plan_group_id = str_replace('gid://shopify/SellingPlanGroup/', '', $sh_sellingPlanGroup['id']);
                      $planGroupData->save();
                    }

                    $sh_sellingPlan = $sh_sellingPlanGroup['sellingPlans']['edges'][$planData->position]['node'];

                    json_encode($sh_sellingPlan);

                    if( !$planData->shopify_plan_id ){
                      $planData->shopify_plan_id = str_replace('gid://shopify/SellingPlan/', '', $sh_sellingPlan['id']);
                      $planData->save();
                    }
                    return 'success';
                } else{
                    logger('============ ERROR:: ShopifySellingPlan User Errors ============');
                    logger( json_encode($sh_sellingplan['userErrors'][0] ) );
                    return $sh_sellingplan['userErrors'][0]['message'];
                }
            }
            return false;
        }catch( \Exception $e ){
            logger('============ ERROR:: ShopifySellingPlan ===========');
            logger(json_encode($e));
        }
    }

    /**
     * @param $planData
     * @return string
     */
    public function createSellingPlanQuery($planData){
        try{
            $plan_gp_id = $planData->ss_plan_group_id;
            $planGroupData = SsPlanGroup::where('id', $plan_gp_id)->first();
            $is_percentage = ($planData->pricing_adjustment_type == '%');
            $adjustmentType = ($is_percentage) ? 'PERCENTAGE' : 'FIXED_AMOUNT';
            $adjustmentValue = ($is_percentage) ? 'percentage: ' : 'fixedValue: ';

             if( $planData->delivery_intent == 'A fixed day each delivery cycle' ){
              if( $planData->delivery_interval == 'day' ){
                 $deliveryPolicy = '{ recurring: { interval: '. strtoupper($planData->delivery_interval) .', intervalCount: '. $planData->delivery_interval_count .', cutoff: '. $planData->delivery_cutoff .', preAnchorBehavior: '. $planData->delivery_pre_cutoff_behaviour .'} }';

                 $billingPolicy = '{ recurring: { interval: '. strtoupper($planData->billing_interval) .', intervalCount: '. $planData->billing_interval_count .', minCycles: '. $planData->billing_min_cycles .', maxCycles: '. $planData->billing_max_cycles .' } }';
              }else{
                if($planData->delivery_anchor_type == 'YEARDAY' ){
                    $deliveryPolicy = '{ recurring: { interval: '. strtoupper($planData->delivery_interval) .', intervalCount: '. $planData->delivery_interval_count .', cutoff: '. $planData->delivery_cutoff .', preAnchorBehavior: '. $planData->delivery_pre_cutoff_behaviour .', anchors: { month: '. $planData->delivery_anchor_month .', type: '. $planData->delivery_anchor_type .'} } }';

                    $billingPolicy = '{ recurring: { interval: '. strtoupper($planData->billing_interval) .', intervalCount: '. $planData->billing_interval_count .', minCycles: '. $planData->billing_min_cycles .', maxCycles: '. $planData->billing_max_cycles .', anchors: { month: '. $planData->delivery_anchor_month .', type: '. $planData->delivery_anchor_type .'} } }';
                }else{
                  $deliveryPolicy = '{ recurring: { interval: '. strtoupper($planData->delivery_interval) .', intervalCount: '. $planData->delivery_interval_count .', cutoff: '. $planData->delivery_cutoff .', preAnchorBehavior: '. $planData->delivery_pre_cutoff_behaviour .', anchors: {day: '. $planData->delivery_anchor_day .', type: '. $planData->delivery_anchor_type .'} } }';

                  $billingPolicy = '{ recurring: { interval: '. strtoupper($planData->billing_interval) .', intervalCount: '. $planData->billing_interval_count .', minCycles: '. $planData->billing_min_cycles .', maxCycles: '. $planData->billing_max_cycles .', anchors: { day: '. $planData->delivery_anchor_day .', type: '. $planData->delivery_anchor_type .'} } }';
                }
                }
            }else{
              $deliveryPolicy = '{ recurring: { interval: '. strtoupper($planData->delivery_interval) .', intervalCount: '. $planData->delivery_interval_count .' } }';

              $billingPolicy = '{ recurring: { interval: '. strtoupper($planData->billing_interval) .', intervalCount: '. $planData->billing_interval_count .', minCycles: '. $planData->billing_min_cycles .', maxCycles: '. $planData->billing_max_cycles .' } }';
            }

            return '
                mutation {
                      sellingPlanGroupCreate(input: {
                            name: "'. $planGroupData->name .'",
                            description: "'. $planGroupData->description .'",
                            merchantCode: "'. $planGroupData->merchantCode .'",
                            position: '. $planGroupData->position .',
                            options: ["'. $planGroupData->options .'"],
                                sellingPlansToCreate: [
                                {
                                  name: "'. $planData->name .'"
                                  description: "'. $planData->description .'"
                                  options: ["'. $planData->options .'"],
                                  position: '. $planData->position .'
                                  billingPolicy: '. $billingPolicy .'
                                  deliveryPolicy: '. $deliveryPolicy .'
                                  pricingPolicies: [
                                    {
                                      fixed: {
                                        adjustmentType: '. $adjustmentType .'
                                        adjustmentValue: { '. $adjustmentValue . $planData->pricing_adjustment_value .'}
                                      }
                                    }
                                  ]
                                }]
                        }) {
                            sellingPlanGroup {
                              id
                              sellingPlans(first: '. ($planData->position + 1) .') {
                                edges {
                                  node {
                                    id
                                  }
                                }
                              }
                            }
                            userErrors {
                              code
                              field
                              message
                            }
                        }
                    }
            ';
        }catch( \Exception $e )
        {
            logger('============ ERROR:: createSellingPlanQuery ===========');
            logger(json_encode($e));
        }
    }

    /**
     * @param $planData
     * @return string
     */
    public function updateSellingPlanQuery($planData){
        try{
            $plan_gp_id = $planData->ss_plan_group_id;
            $planGroupData = SsPlanGroup::where('id', $plan_gp_id)->first();
            $is_percentage = ($planData->pricing_adjustment_type == '%');
            $adjustmentType = ($is_percentage) ? 'PERCENTAGE' : 'FIXED_AMOUNT';
            $adjustmentValue = ($is_percentage) ? 'percentage: ' : 'fixedValue: ';
            $action = ( $planData->shopify_plan_id ) ? 'sellingPlansToUpdate' : 'sellingPlansToCreate';

            if( $planData->delivery_intent == 'A fixed day each delivery cycle' ){
            	if( $planData->delivery_interval == 'day' ){
					       $deliveryPolicy = '{ recurring: { interval: '. strtoupper($planData->delivery_interval) .', intervalCount: '. $planData->delivery_interval_count .', cutoff: '. $planData->delivery_cutoff .', preAnchorBehavior: '. $planData->delivery_pre_cutoff_behaviour .'} }';

					       $billingPolicy = '{ recurring: { interval: '. strtoupper($planData->billing_interval) .', intervalCount: '. $planData->billing_interval_count .', minCycles: '. $planData->billing_min_cycles .', maxCycles: '. $planData->billing_max_cycles .' } }';
            	}else{
            		if($planData->delivery_anchor_type == 'YEARDAY' ){
						        $deliveryPolicy = '{ recurring: { interval: '. strtoupper($planData->delivery_interval) .', intervalCount: '. $planData->delivery_interval_count .', cutoff: '. $planData->delivery_cutoff .', preAnchorBehavior: '. $planData->delivery_pre_cutoff_behaviour .', anchors: { month: '. $planData->delivery_anchor_month .', type: '. $planData->delivery_anchor_type .'} } }';

						        $billingPolicy = '{ recurring: { interval: '. strtoupper($planData->billing_interval) .', intervalCount: '. $planData->billing_interval_count .', minCycles: '. $planData->billing_min_cycles .', maxCycles: '. $planData->billing_max_cycles .', anchors: { month: '. $planData->delivery_anchor_month .', type: '. $planData->delivery_anchor_type .'} } }';
            		}else{
            			$deliveryPolicy = '{ recurring: { interval: '. strtoupper($planData->delivery_interval) .', intervalCount: '. $planData->delivery_interval_count .', cutoff: '. $planData->delivery_cutoff .', preAnchorBehavior: '. $planData->delivery_pre_cutoff_behaviour .', anchors: {day: '. $planData->delivery_anchor_day .', type: '. $planData->delivery_anchor_type .'} } }';

            			$billingPolicy = '{ recurring: { interval: '. strtoupper($planData->billing_interval) .', intervalCount: '. $planData->billing_interval_count .', minCycles: '. $planData->billing_min_cycles .', maxCycles: '. $planData->billing_max_cycles .', anchors: { day: '. $planData->delivery_anchor_day .', type: '. $planData->delivery_anchor_type .'} } }';
            		}
            		}
            }else{
            	$deliveryPolicy = '{ recurring: { interval: '. strtoupper($planData->delivery_interval) .', intervalCount: '. $planData->delivery_interval_count .' } }';
            	$billingPolicy = '{ recurring: { interval: '. strtoupper($planData->billing_interval) .', intervalCount: '. $planData->billing_interval_count .', minCycles: '. $planData->billing_min_cycles .', maxCycles: '. $planData->billing_max_cycles .' } }';
            }

            logger($deliveryPolicy);
            $query= '
                mutation {
                      sellingPlanGroupUpdate(id: "gid://shopify/SellingPlanGroup/'. $planGroupData->shopify_plan_group_id .'", input: {
                            name: "'. $planGroupData->name .'",
                            description: "'. $planGroupData->description .'",
                            merchantCode: "'. $planGroupData->merchantCode .'",
                            position: '. $planGroupData->position .',
                            options: ["'. $planGroupData->options .'"],
                                '. $action .': [
                                {';

            $query .= ( $planData->shopify_plan_id ) ? 'id: "gid://shopify/SellingPlan/' . $planData->shopify_plan_id .'", ' : '';
            $query .=   'name: "'. $planData->name .'"
                                  description: "'. $planData->description .'"
                                  options: ["'. $planData->options .'"]
                                  position: '. $planData->position .'
                                  billingPolicy: '. $billingPolicy .'
                                  deliveryPolicy: '. $deliveryPolicy .'
                                  pricingPolicies: [
                                    {
                                      fixed: {
                                        adjustmentType: '. $adjustmentType .'
                                        adjustmentValue: { '. $adjustmentValue . $planData->pricing_adjustment_value .'}
                                      }
                                    }
                                  ]
                                }]
                        }) {
                            sellingPlanGroup {
                              id
                              sellingPlans(first: '. ($planData->position + 1) .') {
                                edges {
                                  node {
                                    id
                                  }
                                }
                              }
                            }
                            userErrors {
                              code
                              field
                              message
                            }
                        }
                    }
            ';
            return $query;
        }catch( \Exception $e )
        {
            logger('============ ERROR:: updateSellingPlanQuery ===========');
            logger(json_encode($e));
        }
    }

    public function updateSellingPlanGroup($user_id, $planGroupData){
        try{

           $query= '
                mutation {
                    sellingPlanGroupUpdate(id: "gid://shopify/SellingPlanGroup/'. $planGroupData->shopify_plan_group_id .'", input: {
                            name: "'. $planGroupData->name .'",
                            description: "'. $planGroupData->description .'",
                            position: '. $planGroupData->position .',
                    }){
                      userErrors {
                        code
                        field
                        message
                      }
                    }
                }
            ';

            logger($query);
            $result = $this->graphQLRequest($user_id, $query);
            $msg = $this->getReturnMessage($result, 'SellingPlanGroup');
            return $msg;
        }catch( \Exception $e )
        {
            logger('============ ERROR:: updateSellingPlanGroup ===========');
            logger(json_encode($e));
        }
    }

    public function updateSellingPlan($user_id, $planGroupID, $planData){
        try{

           $query= '
                mutation {
                    sellingPlanGroupUpdate(id: "gid://shopify/SellingPlanGroup/'. $planGroupID .'", input: {
                            sellingPlansToUpdate: [{
                                id: "gid://shopify/SellingPlan/'. $planData->shopify_plan_id .'", position: '. $planData->position .'
                            }]
                    }){
                      userErrors {
                        code
                        field
                        message
                      }
                    }
                }
            ';

            logger($query);
            $result = $this->graphQLRequest($user_id, $query);
            $msg = $this->getReturnMessage($result, 'SellingPlanGroup');
            return $msg;
        }catch( \Exception $e )
        {
            logger('============ ERROR:: updateSellingPlan ===========');
            logger(json_encode($e));
        }
    }

    public function updateSellingPlanGroupProduct($user_id, $productIds, $planGroupID){
        try{
           $query= '
                mutation {
                    sellingPlanGroupAddProducts(id: "gid://shopify/SellingPlanGroup/'. $planGroupID .'", productIds: ["gid://shopify/Product/'. implode('","gid://shopify/Product/', $productIds) .'"]) {
                      userErrors {
                        code
                        field
                        message
                      }
                    }
                }
            ';

            $result =  $this->graphQLRequest($user_id, $query);
            $msg = $this->getReturnMessage($result);
            return $msg;
        }catch( \Exception $e )
        {
            logger('============ ERROR:: updateSellingPlanGroupProduct ===========');
            logger(json_encode($e));
        }
    }

    public function getReturnMessage($result, $action){
      if( !$result['errors'] ){
          $data = $result['body']->container['data'][$action];
          if( empty($data['userErrors'] )){
             return 'success';
          }else{
            return $data['userErrors'][0]['message'];
          }
      }
      return false;
    }

    public function createOrder($user_id, $shop_id, $shopify_order_id, $customer_id, $contract_id){
        $user = User::find($user_id);
        $endPoint = 'admin/api/'.env('SHOPIFY_API_VERSION').'/orders/'.$shopify_order_id.'.json';
        $result = $user->api()->rest('GET', $endPoint);
        if (!$result['errors']) {
            $data = $result['body']['order'];
            logger(json_encode($data));
            $db_order = new SsOrder;
            $plan = Plan::find($user->plan_id);

            $db_rates = ExchangeRate::orderBy('created_at', 'desc')->first();
            $rate = json_decode($db_rates->conversion_rates);
            $currencyCode = $data->currency;

            $tx_fee = $plan->transaction_fee;
            $db_order->shop_id = $shop_id;
            $db_order->user_id = $user->id;
            $db_order->shopify_order_id = $shopify_order_id;
            $db_order->ss_customer_id = $customer_id;
            $db_order->ss_contract_id = $contract_id;
            $db_order->shopify_order_name = $data->name;
            $db_order->order_currency = $data->currency;
            $db_order->currency_symbol = currencyH($data->currency);
            $db_order->order_amount = calculateCurrency($data->currency, 'USD', $data->total_price);
            $db_order->conversion_rate = $rate->$currencyCode;
            $db_order->tx_fee_status = 'pending';
            $db_order->tx_fee_percentage = $tx_fee;
            $db_order->tx_fee_amount = number_format(($db_order->order_amount * $tx_fee), 4);
            $db_order->save();
        }
    }

    public function getSubscriptionType($shop_id){
        $setting = SsSetting::select('subscription_daily_at')->where('shop_id', $shop_id)->first();
        $subscription_daily_at = $setting->subscription_daily_at;
        $timeformat = (substr($subscription_daily_at, -2));
        $time = substr($subscription_daily_at, 0, 5);

        if ($timeformat == 'PM' && $time == '11:59') {
            $addtime = 24 .':00:00';
        } else {
            if ($timeformat == 'PM' && $time == '12:01') {
                $addtime = 1 .':00:00';
            } else {
                if ($timeformat == 'PM' && $time != '12:01') {
                    $addtime = (12 + substr($subscription_daily_at, 0, 2)).':00:00';
                } else {
                    $addtime = substr($subscription_daily_at, 0, 2).':00:00';
                }
            }
        }
        return $addtime;
    }


    public function getShopifyOrder($user, $orderId){
        $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/orders/' . $orderId . '.json';
        $parameter['fields'] = 'id,total_price';
        $result = $user->api()->rest('GET', $endPoint, $parameter);
        if( !$result['errors'] ){
            return $result['body']->container['order'];
        }else{
            return [];
        }
    }
    public function updateSubscriptionContract($user_id, $contract){
      try{
        logger('========= START:: updateSubscriptionContract =========');
        $draftId = $this->getSubscriptionDraft($user_id, $shopify_contract_id->shopify_contract_id);
        if( $draftId ){
          $draftQuery = '
                  mutation{
                    subscriptionDraftUpdate(draftId: "'.$draftId.'",
                    input: { status: '. strtoupper($contract->status) .' }
                    ) {
                      userErrors {
                        code
                        field
                        message
                      }
                      draft {
                        status
                      }
                    }
                  }
              ';
              $result =  $this->graphQLRequest($user->id, $draftQuery);
              $message = $this->getReturnMessage($resultSubscriptionContract, 'subscriptionDraftUpdate');
              return $message;
        }
      }catch( \Exception $e ){
        logger('========= ERROR:: updateSubscriptionContract =========');
        logger(json_encode($e));
      }
    }

    public function getSubscriptionDraft($user_id, $shopify_contract_id){
        $user = User::find($user_id);
       // get draft id from contract id
         $query = 'mutation {
          subscriptionContractUpdate(contractId: "gid://shopify/SubscriptionContract/'.$contractId.'") {
              draft {
                id
              }
              userErrors {
                  message
              }
            }
          }';
          $resultSubscriptionContract =  $this->graphQLRequest($user->id, $query);
          $message = $this->getReturnMessage($resultSubscriptionContract, 'SubscriptionContract');
          if( $message == 'success' ){
            $subscriptionContract = $resultSubscriptionContract['body']->container['data']['subscriptionContractUpdate'];
            $draftId = (@$subscriptionContract['draft']['id']) ? $subscriptionContract['draft']['id'] : '';
            return $draftId;
          }else{
            return $message;
          }
    }


    public function getCustomerPaymentMethodID($user_id, $contractID){
      try{
          logger('========= START:: getCustomerPaymentMethodID =========');
          $paymentMethodID = '';
          $query = '{
                     subscriptionContract(id: "gid://shopify/SubscriptionContract/'. $contractID .'") {
                        customerPaymentMethod {
                          id
                        }
                    }
                }';

        $result =  $this->graphQLRequest($user_id, $query);
        if( !$result['errors'] ){
          $subContract = $result['body']->container['data']['subscriptionContract'];
          $paymentMethodID = (@$subContract['customerPaymentMethod']['id']) ? $subContract['customerPaymentMethod']['id'] : '';
        }else{
            logger('============== getCustomerPaymentMethodID ===============');
            logger(json_encode($result));
        }
        return $paymentMethodID;
      }catch ( \Exception $e ){
          logger('========= ERROR:: getCustomerPaymentMethodID =========');
          logger(json_encode($e));
      }
    }

    public function createCustomerPaymentMethodSendUpdateEmail($user_id, $contractID){
      try{
          logger('========= START:: createCustomerPaymentMethodSendUpdateEmail =========');
          $paymentMethodID = $this->getCustomerPaymentMethodID($user_id, $contractID);
          if( $paymentMethodID != '' ){
              $query = 'mutation{
                          customerPaymentMethodSendUpdateEmail(customerPaymentMethodId: "'.$paymentMethodID.'") {
                            customer {
                              id
                            }
                            userErrors {
                              field
                              message
                            }
                        }
                      }';
            $result =  $this->graphQLRequest($user_id, $query);
            $message = $this->getReturnMessage($result, 'customerPaymentMethodSendUpdateEmail');
          }else{
            $message = 'Customer payment method not found';
          }

          return $message;
      }catch ( \Exception $e ){
          logger('========= ERROR:: createCustomerPaymentMethodSendUpdateEmail =========');
          logger(json_encode($e));
      }
    }

    public function getPrepaidFulfillments($user_id, $last_billing_order){
      try{
          logger('========= START:: getPrepaidFulfillments =========');
          $query = '{
                    order(id: "gid://shopify/Order/'. $last_billing_order .'") {
                      fulfillmentOrders(first: 250) {
                        edges {
                          node {
                            fulfillAt
                            status
                            id
                            order {
                              name
                              legacyResourceId
                            }
                          }
                        }
                      }
                    }
                  }';
         $result =  $this->graphQLRequest($user_id, $query);
         if( !$result['errors'] ){
            $fulfillmentOrders = $result['body']->container['data']['order']['fulfillmentOrders'];
            return ( $fulfillmentOrders ) ? $fulfillmentOrders['edges'] : [];
         }else{
            logger('========= USER ERROR:: getPrepaidFulfillments =========');
            logger(json_encode($result));
            return [];
         }
        }catch ( \Exception $e ){
          logger('========= ERROR:: getPrepaidFulfillments =========');
          logger(json_encode($e));
      }
    }
}
