<?php

namespace App\Listeners;

use App\Events\CheckSubscriptionContract;
use App\Models\ExchangeRate;
use App\Models\Shop;
use App\Models\Ss_contract;
use App\Models\Ss_customer;
use App\Models\SsBillingAttempt;
use App\Models\SsContractLineItem;
use App\Models\SsOrder;
use App\Models\SsPlan;
use App\Models\SsSetting;
use App\Models\SsWebhook;
use App\Traits\ShopifyTrait;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Osiset\ShopifyApp\Storage\Models\Plan;

class SubscriptionContract
{
    use ShopifyTrait;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckSubscriptionContract  $event
     * @return void
     */
    public function handle(CheckSubscriptionContract $event)
    {
        logger('========== Listener:: SubscriptionContract ==========');
        $ids = $event->ids;
        $user = User::find($ids['user_id']);
        $shop = Shop::find($ids['shop_id']);
        $webhookResonse = SsWebhook::find($ids['webhook_id']);
        if ($webhookResonse) {
            $data = json_decode($webhookResonse->body);
            // update or create customer in db
            $shopify_customer_id = $data->customer_id;
            $endPoint = 'admin/api/'.env('SHOPIFY_API_VERSION').'/customers/'.$shopify_customer_id.'.json';
            $result = $user->api()->rest('GET', $endPoint);
            if (!$result['errors']) {
                $sh_customer = $result['body']['customer'];

                $is_existcustomer = Ss_customer::where('shop_id', $ids['shop_id'])->where('shopify_customer_id',
                    $shopify_customer_id)->first();
                $customer = ($is_existcustomer) ? $is_existcustomer : new Ss_customer;
                // $customer = new Ss_customer;
                $customer->shop_id = $ids['shop_id'];
                $customer->shopify_customer_id = $sh_customer->id;
                $customer->active = 1;
                $customer->first_name = $sh_customer->first_name;
                $customer->last_name = $sh_customer->last_name;
                $customer->email = $sh_customer->email;
                $customer->phone = $sh_customer->phone;
                $customer->notes = $sh_customer->note;
                $customer->total_orders = ( $is_existcustomer ) ? $is_existcustomer->total_orders + 1 : $sh_customer->orders_count;

                $customer->total_spend_currency = $sh_customer->currency;
                $customer->currency_symbol = currencyH($sh_customer->currency);
                $customer->avg_order_value = ($sh_customer->orders_count > 0) ? ($sh_customer->total_spent / $sh_customer->orders_count) : 0;
                $customer->date_first_order = ($is_existcustomer && $is_existcustomer->date_first_order) ? $is_existcustomer->date_first_order : date('Y-m-d H:i:s');
                $customer->save();
            }

            // get subscription contract from shopify

            $ssContractQuery = $this->subscriptionContractLineItems($data->admin_graphql_api_id);
            $ssContractResult = $this->graphQLRequest($user->id, $ssContractQuery);

            logger(json_encode($ssContractResult));
            if (!$ssContractResult['errors']) {
                $subscriptionContract = $ssContractResult['body']->container['data']['subscriptionContract'];

                $sh_lines = (@$subscriptionContract['lines']['edges']) ? $subscriptionContract['lines']['edges'] : [];
                $sh_cpm = (@$subscriptionContract['customerPaymentMethod']['instrument']) ? $subscriptionContract['customerPaymentMethod']['instrument'] : [];
                $sh_deliveryMethod = (@$subscriptionContract['deliveryMethod']) ? $subscriptionContract['deliveryMethod'] : [];

                // add contract

                $billing_policy = $data->billing_policy->interval_count.$data->billing_policy->interval;
                $delivery_policy = $data->delivery_policy->interval_count.$data->delivery_policy->interval;

                $contract = new Ss_contract;
                $contract->shop_id = $ids['shop_id'];
                $contract->user_id = $user->id;
                $contract->shopify_contract_id = $data->id;
                $contract->shopify_customer_id = $shopify_customer_id;
                $contract->ss_customer_id = $customer->id;
                $contract->origin_order_id = $data->origin_order_id;
                $contract->status = $data->status;

                $default_timezone = date_default_timezone_get();
                date_default_timezone_set($shop->iana_timezone);

                $addtime = $this->getSubscriptionType($shop->id);

                $next_order_date = date("Y-m-d $addtime",
                    strtotime($subscriptionContract['nextBillingDate']));
                $contract->next_order_date = $next_order_date;
                $contract->next_processing_date = $next_order_date;

                date_default_timezone_set($default_timezone);

                $deliveryAnchors = $subscriptionContract['deliveryPolicy']['anchors'];
                $billingAnchors = $subscriptionContract['billingPolicy']['anchors'];

                $contract->is_prepaid = !strcmp($billing_policy, $delivery_policy);
                $contract->prepaid_renew = 0;
                $contract->last_billing_order_number = $data->origin_order_id;

                $contract->billing_interval = $data->billing_policy->interval;
                $contract->billing_interval_count = $data->billing_policy->interval_count;
                $contract->billing_min_cycles = $data->billing_policy->min_cycles;
                $contract->billing_max_cycles = $data->billing_policy->max_cycles;
                $contract->billing_anchor_day = ( !empty($billingAnchors) ) ? $billingAnchors[0]['day']  : null;
                $contract->billing_anchor_type = ( !empty($billingAnchors) ) ? $billingAnchors[0]['type']  : null;
                $contract->billing_anchor_month = ( !empty($billingAnchors) ) ? $billingAnchors[0]['month']  : null;

                $contract->delivery_anchor_day = ( !empty($deliveryAnchors) ) ? $deliveryAnchors[0]['day']  : null;
                $contract->delivery_anchor_type = ( !empty($deliveryAnchors) ) ? $deliveryAnchors[0]['type']  : null;
                $contract->delivery_anchor_month = ( !empty($deliveryAnchors) ) ? $deliveryAnchors[0]['month']  : null;

                $contract->delivery_intent = ( !empty($deliveryAnchors) ) ? 'A fixed day each delivery cycle' : 'The initial day of purchase';
                $contract->delivery_interval = $data->delivery_policy->interval;
                $contract->delivery_interval_count = $data->delivery_policy->interval_count;
                $contract->currency_code = $data->currency_code;
                $contract->lastPaymentStatus = $subscriptionContract['lastPaymentStatus'];
                $contract->order_count = 1;

                $contract->cc_id = str_replace('gid://shopify/CustomerPaymentMethod/', '',
                    $subscriptionContract['customerPaymentMethod']['id']);
                $contract->cc_brand = $sh_cpm['brand'];
                $contract->cc_expiryMonth = $sh_cpm['expiryMonth'];
                $contract->cc_expiryYear = $sh_cpm['expiryYear'];
                $contract->cc_firstDigits = $sh_cpm['firstDigits'];
                $contract->cc_lastDigits = $sh_cpm['lastDigits'];
                $contract->cc_maskedNumber = $sh_cpm['maskedNumber'];
                $contract->cc_name = $sh_cpm['name'];

                $ship_address = (@$sh_deliveryMethod['address']) ? $sh_deliveryMethod['address'] : [];

                $contract->ship_company = (@$ship_address['company']) ? $ship_address['company'] : '';
                $contract->ship_firstName = (@$ship_address['firstName']) ? $ship_address['firstName'] : '';
                $contract->ship_lastName = (@$ship_address['lastName']) ? $ship_address['lastName'] : '';
                $contract->ship_provinceCode = (@$ship_address['provinceCode']) ? $ship_address['provinceCode'] : '';
                $contract->ship_name = (@$ship_address['name']) ? $ship_address['name'] : '';
                $contract->ship_address1 = (@$ship_address['address1']) ? $ship_address['address1'] : '';
                $contract->ship_address2 = (@$ship_address['address2']) ? $ship_address['address2'] : '';
                $contract->ship_city = (@$ship_address['city']) ? $ship_address['city'] : '';
                $contract->ship_province = (@$ship_address['province']) ? $ship_address['province'] : '';
                $contract->ship_zip = (@$ship_address['zip']) ? $ship_address['zip'] : '';
                $contract->ship_country = (@$ship_address['country']) ? $ship_address['country'] : '';
                $contract->ship_phone = (@$ship_address['phone']) ? $ship_address['phone'] : '';

                $ship_option = (@$sh_deliveryMethod['shippingOption']) ? $sh_deliveryMethod['shippingOption'] : [];

                $contract->shipping_code = (@$ship_option['code']) ? $ship_option['code'] : '';
                $contract->shipping_description = (@$ship_option['description']) ? $ship_option['description'] : '';
                $contract->shipping_presentmentTitle = (@$ship_option['presentmentTitle']) ? $ship_option['presentmentTitle'] : '';
                $contract->shipping_title = (@$ship_option['title']) ? $ship_option['title'] : '';

                $contract->failed_payment_count = 0;

                $contract->save();
                // add contract line items

                if (is_array($sh_lines) && !empty($sh_lines)) {
                    foreach ($sh_lines as $key => $value) {
                        $node = $value['node'];
                        $db_plan = SsPlan::where('shop_id', $shop->id)->where('user_id',
                            $user->id)->where('shopify_plan_id',
                            str_replace('gid://shopify/SellingPlan/', '', $node['sellingPlanId']))->first();
                        $pricingPolicy = $node['pricingPolicy'];

                        if ($db_plan) {
                            $pricing_adjustment_type = $db_plan->pricing_adjustment_type;
                            $pricing_adjustment_value = $db_plan->pricing_adjustment_value;

                            $price = $pricingPolicy['basePrice']['amount'];

                            $discount = 0;
                            if ($pricing_adjustment_value != null && $pricing_adjustment_value != '') {
                                if ($pricing_adjustment_type == '%') {
                                    $discount = (($price * $pricing_adjustment_value) / 100);
                                } else {
                                    $discount = $pricing_adjustment_value;
                                }
                            }
                            $discounted_price = $price - $discount;
                        }

                        $lineItems = new SsContractLineItem;
                        $lineItems->shopify_contract_id = $data->id;
                        $lineItems->ss_contract_id = $contract->id;
                        $lineItems->user_id = $user->id;
                        $lineItems->shopify_product_id = str_replace('gid://shopify/Product/', '', $node['productId']);
                        $lineItems->shopify_variant_id = str_replace('gid://shopify/ProductVariant/', '',
                            $node['variantId']);
                        $lineItems->price = number_format($pricingPolicy['basePrice']['amount'], 2);
                        $lineItems->price_discounted = number_format($discounted_price, 2);
                        $lineItems->currency = $node['currentPrice']['currencyCode'];
                        $lineItems->currency_symbol = currencyH($node['currentPrice']['currencyCode']);
                        $lineItems->discount_type = ($pricingPolicy['cycleDiscounts'][0]['adjustmentType'] == 'PERCENTAGE') ? '%' : currencyH($node['currentPrice']['currencyCode']);
                        $lineItems->discount_amount = number_format($discount, 2);
                        $lineItems->final_amount = $discounted_price;
                        $lineItems->quantity = $node['quantity'];
                        $lineItems->selling_plan_id = str_replace('gid://shopify/SellingPlan/', '',
                            $node['sellingPlanId']);
                        $lineItems->selling_plan_name = $node['sellingPlanName'];
                        $lineItems->sku = $node['sku'];
                        $lineItems->taxable = $node['taxable'];
                        $lineItems->title = $node['title'];
                        $lineItems->shopify_variant_image = $node['variantImage']['originalSrc'];
                        $lineItems->shopify_variant_title = $node['variantTitle'];
                        $lineItems->requiresShipping = $node['requiresShipping'];

                        $lineItems->save();

                    }
                }
            }

            //update customer
            if( $customer ) {
                $sh_order = $this->getShopifyOrder($user, $data->origin_order_id);
                $customer->total_spend = (@$sh_order['total_price']) ? $customer->total_spend + $sh_order['total_price'] : $customer->total_spend;
                $customer->avg_order_value = number_format(($customer->total_spend / $customer->total_orders), 2);
                $customer->save();
            }
                // create order
            $this->createOrder($user->id, $shop->id, $data->origin_order_id, $customer->id, $contract->id);

            //add billing attempt

            $billingAttempt = new SsBillingAttempt;
            $billingAttempt->shop_id = $shop->id;
            $billingAttempt->ss_contract_id = $contract->id;
            $billingAttempt->status = 'successful';
            $billingAttempt->completedAt = date('Y-m-d H:i:s');
            $billingAttempt->shopify_contract_id = $contract->shopify_contract_id;
            $billingAttempt->shopify_order_id = $contract->origin_order_id;
            $billingAttempt->save();
        }
    }

    public function subscriptionContractLineItems($contractId)
    {
        $query = '
                {
                        subscriptionContract(id: "'.$contractId.'") {
                          lastPaymentStatus
                          nextBillingDate
                          billingPolicy {
                              interval
                              intervalCount
                              maxCycles
                              minCycles
                              anchors {
                                day
                                month
                                type
                              }
                            }
                            deliveryPolicy {
                              interval
                              intervalCount
                              anchors {
                                day
                                month
                                type
                              }
                            }
                            lines (first: 200){
                              edges {
                                node {
                                  id
                                  productId
                                  variantId
                                  currentPrice {
                                    amount
                                    currencyCode
                                  }
                                  pricingPolicy {
                                    basePrice {
                                      amount
                                      currencyCode
                                    }
                                    cycleDiscounts {
                                      adjustmentType
                                      afterCycle
                                      computedPrice {
                                        amount
                                        currencyCode
                                      }
                                    }
                                  }
                                  variantImage {
                                    originalSrc
                                  }
                                  sku
                                  title
                                  quantity
                                  taxable
                                  sellingPlanName
                                  sellingPlanId
                                  requiresShipping
                                  variantTitle
                                  lineDiscountedPrice {
                                    amount
                                    currencyCode
                                  }
                                }
                              }
                            }
                            deliveryMethod {
                            ... on SubscriptionDeliveryMethodShipping {
                              __typename
                              address {
                                address1
                                address2
                                city
                                company
                                country
                                countryCode
                                firstName
                                lastName
                                name
                                phone
                                province
                                provinceCode
                                zip
                              }
                              shippingOption {
                                code
                                description
                                presentmentTitle
                                title
                              }
                            }
                          }
                          customerPaymentMethod {
                              id
                              instrument {
                                ... on CustomerCreditCard {
                                  firstDigits
                                  brand
                                  expiresSoon
                                  expiryMonth
                                  expiryYear
                                  isRevocable
                                  lastDigits
                                  maskedNumber
                                  name
                                }
                              }
                              revokedAt
                            }
                      }
                }
            ';
        return $query;
    }
}
