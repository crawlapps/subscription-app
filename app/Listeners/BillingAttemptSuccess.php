<?php

namespace App\Listeners;

use App\Events\CheckBillingAttemptSuccess;
use App\Models\ExchangeRate;
use App\Models\Shop;
use App\Models\Ss_contract;
use App\Models\Ss_customer;
use App\Models\SsBillingAttempt;
use App\Models\SsOrder;
use App\Models\SsWebhook;
use App\Traits\ShopifyTrait;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Osiset\ShopifyApp\Storage\Models\Plan;

class BillingAttemptSuccess
{
    use ShopifyTrait;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckBillingAttemptSuccess  $event
     * @return void
     */
    public function handle(CheckBillingAttemptSuccess $event)
    {
        try {
            logger('========== Listener:: BillingAttemptSuccess ==========');
            $ids = $event->ids;
            $user = User::find($ids['user_id']);
            $shop = Shop::find($ids['shop_id']);
            $webhookResonse = SsWebhook::find($ids['webhook_id']);

            if( $webhookResonse ){
                $data = json_decode($webhookResonse->body);

//                update billing attempt
                $ssBillingAttempt = SsBillingAttempt::where('shop_id', $shop->id)->where('shopify_id', $data->id)->first();
                if( $ssBillingAttempt ){
                    $ssBillingAttempt->status = 'successful';
                    $ssBillingAttempt->completedAt = date('Y-m-d H:i:s');
                    $ssBillingAttempt->shopify_order_id = $data->order_id;
                    $ssBillingAttempt->save();
                }

//                update contract
                $ssContract = Ss_contract::where('shop_id', $shop->id)->where('shopify_contract_id', $data->subscription_contract_id)->first();
                if( $ssContract ){
                    $default_timezone = date_default_timezone_get();
                    date_default_timezone_set($shop->iana_timezone);

                    $addtime = $this->getSubscriptionType($shop->id);

                    $next_order_date = date("Y-m-d $addtime", strtotime($ssContract->next_order_date . ' + ' . $ssContract->billing_interval_count . ' ' . strtolower($ssContract->billing_interval)));

                    date_default_timezone_set($default_timezone);

                    $ssContract->status_billing = 'successful';
                    $ssContract->error_state = null;
                    $ssContract->next_order_date = $next_order_date;
                    $ssContract->next_processing_date = $next_order_date;
                    $ssContract->last_billing_order_number = $data->order_id;
                    $ssContract->order_count = $ssContract->order_count++;
                    $ssContract->failed_payment_count = 0;
                    $ssContract->save();

                    //update customer
                    $ssCustomer = Ss_customer::where('shop_id', $shop->id)->where('shopify_customer_id', $ssContract->shopify_customer_id)->first();
                    if( $ssCustomer ){
                        $sh_order = $this->getShopifyOrder($user, $data->order_id);
                        $ssCustomer->total_orders = $ssCustomer->total_orders + 1;
                        $ssCustomer->total_spend = (@$sh_order['total_price']) ? $ssCustomer->total_spend + $sh_order['total_price'] : '';
                        $ssCustomer->avg_order_value = number_format(($ssCustomer->total_spend / $ssCustomer->total_orders), 2);
                        $ssCustomer->save();

                        // create order
                        $this->createOrder($user->id, $shop->id, $data->order_id, $ssCustomer->id, $ssContract->id);

                    }
                }
            }
        }catch ( \Exception $e ){
            logger('========== ERROR:: Listener:: BillingAttemptSuccess ==========');
            logger(json_encode($e));
        }
    }
}
