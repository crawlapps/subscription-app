<?php

namespace App\Listeners;

use App\Events\CheckBillingAttemptFailure;
use App\Models\Shop;
use App\Models\Ss_contract;
use App\Models\Ss_customer;
use App\Models\SsActivityLog;
use App\Models\SsBillingAttempt;
use App\Models\SsEmail;
use App\Models\SsSetting;
use App\Models\SsWebhook;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class BillingAttemptFailure
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckBillingAttemptFailure  $event
     * @return void
     */
    public function handle(CheckBillingAttemptFailure $event)
    {
        try {
            logger('========== Listener:: BillingAttemptFailure ==========');
            $ids = $event->ids;
            $user = User::find($ids['user_id']);
            $shop = Shop::find($ids['shop_id']);
            $webhookResonse = SsWebhook::find($ids['webhook_id']);

            if( $webhookResonse ){
                $data = json_decode($webhookResonse->body);

                $setting = SsSetting::select('dunning_retries', 'dunning_daysbetween', 'dunning_failedaction', 'dunning_email_enabled', 'email_from_email', 'email_from_name')->where('shop_id', $shop->id)->first();
                if( $setting ){
                    $ssContract = Ss_contract::where('shop_id', $shop->id)->where('shopify_contract_id', $data->subscription_contract_id)->first();

                    $contract_failed_payment_count = $ssContract->failed_payment_count;
                    $setting_dunning_retries = $setting->dunning_retries;

                    if( $contract_failed_payment_count == ( $setting_dunning_retries - 1 ) ){
                        $ssContract->failed_payment_count = $ssContract->failed_payment_count + 1;

                        $ss_activity_log = new SsActivityLog;
                        $ss_activity_log->shop_id = $shop->id;
                        $ss_activity_log->user_id = $user->id;
                        $ss_activity_log->ss_contract_id = $ssContract->id;
                        $ss_activity_log->ss_customer_id = $ssContract->ss_customer_id;
                        $ss_activity_log->user_type = 'System';
                        $ss_activity_log->user_name = $shop->owner;
                        $ss_activity_log->user_name = $shop->owner;

                        if( $setting->dunning_failedaction == 'cancel' ){
                            $ssContract->status = 'cancelled';
                            $ss_activity_log->message = "Subscription [contract #$data->subscription_contract_id] cancelled after reaching max number of failed billing attempts";
                        }elseif( $setting->dunning_failedaction == 'pause' ){
                            $ssContract->status = 'paused';
                            $ss_activity_log->message = "Subscription [contract #$data->subscription_contract_id] paused after reaching max number of failed billing attempts";
                        }elseif( $setting->dunning_failedaction == 'skip' ){
                            $next_order_date = date('Y-m-d H:i:s', strtotime($ssContract->next_order_date . ' + ' . $ssContract->billing_interval_count . ' ' . strtolower($ssContract->billing_interval)));

                            $ssContract->next_order_date = $next_order_date;
                            $ssContract->next_processing_date = $next_order_date;

                            $ss_activity_log->message = "Subscription [contract #$data->subscription_contract_id] skipped an order after reaching max number of failed billing attempts";
                        }
                        $ssContract->save();
                        $ss_activity_log->save();
                    }else if( $contract_failed_payment_count > 0 && $contract_failed_payment_count < $setting_dunning_retries ){

                        $increaseDay = $setting->dunning_daysbetween + 1;
                        $ssContract->failed_payment_count = $ssContract->failed_payment_count + 1;
                        $ssContract->next_processing_date = date('Y-m-d H:i:s', strtotime($ssContract->next_processing_date . ' + ' . $increaseDay . ' day'));
                        $ssContract->save();

    //                update billing attempt
                        $ssBillingAttempt = SsBillingAttempt::where('shop_id', $shop->id)->where('shopify_id', $data->id)->first();
                        if( $ssBillingAttempt ){
                            $ssBillingAttempt->status = 'failed';
                            $ssBillingAttempt->completedAt = date('Y-m-d H:i:s');
                            $ssBillingAttempt->save();
                        }

                        //send mail
                        if( $setting->dunning_email_enabled ){
                            $email = SsEmail::where('shop_id', $shop->id)->where('category', 'failed_payment_to_customer')->first();
                            $customer = Ss_customer::select('email')->where('shop_id', $shop->id)->where('shopify_customer_id', $ssContract->shopify_customer_id)->first();

                            $res = sendMailH($email->subject, $data->html_body, $setting->email_from_email, $customer->email, $setting->email_from_name);
                            logger('============ Failed payment customer email ============');
                            logger(json_encode($res));
                        }
                    }else if( ($contract_failed_payment_count >= $setting_dunning_retries) &&  $setting->dunning_failedaction == 'skip'){
                        $ssContract->failed_payment_count = $ssContract->failed_payment_count + 1;

                        $ss_activity_log = new SsActivityLog;
                        $ss_activity_log->shop_id = $shop->id;
                        $ss_activity_log->user_id = $user->id;
                        $ss_activity_log->ss_contract_id = $ssContract->id;
                        $ss_activity_log->ss_customer_id = $ssContract->ss_customer_id;
                        $ss_activity_log->user_type = 'System';
                        $ss_activity_log->user_name = $shop->owner;
                        $ss_activity_log->user_name = $shop->owner;

                        $next_order_date = date('Y-m-d H:i:s', strtotime($ssContract->next_order_date . ' + ' . $ssContract->billing_interval_count . ' ' . strtolower($ssContract->billing_interval)));

                        $ssContract->next_order_date = $next_order_date;
                        $ssContract->next_processing_date = $next_order_date;

                        $ss_activity_log->message = "Subscription [contract #$data->subscription_contract_id] skipped an order after reaching max number of failed billing attempts";

                        $ssContract->save();
                        $ss_activity_log->save();
                    }
                }
            }
        }catch ( \Exception $e ){
            logger('========== ERROR:: Listener:: BillingAttemptFailure ==========');
            logger(json_encode($e));
        }
    }
}
