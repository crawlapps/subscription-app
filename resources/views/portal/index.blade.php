<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Subscription</title>

         <!-- Scripts -->
        <script src="{{ asset('js/portal-app.js') }}" defer></script>
        <!-- style custom css -->
        <link href="{{ asset('css/portal-app.css') }}" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@5.12.0/dist/styles.css"/>
        <!-- font-awesome CSS -->
        <script src="https://kit.fontawesome.com/7945acc650.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div id="portal">
            <index></index>
        </div>
    </body>
</html>
