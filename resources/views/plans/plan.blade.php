<!DOCTYPE html>
<html>
<head>

    <!-- Meta tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- style custom css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">

    <!-- Polaris css cdn -->
    <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@5.1.0/dist/styles.css"/>

    <!-- font-awesome link -->
    <script src="https://kit.fontawesome.com/7945acc650.js" crossorigin="anonymous"></script>

    <title>Subscriptions</title>
    @if(config('shopify-app.appbridge_enabled'))
        <script src="https://unpkg.com/@shopify/app-bridge"></script>
        <script>
            var AppBridge = window['app-bridge'];
            var createApp = AppBridge.default;

            window.shopify_app_bridge = createApp({
                apiKey: '{{ config('shopify-app.api_key') }}',
                shopOrigin: '{{ Auth::user()->name }}',
                forceRedirect: true,
            });
        </script>
    @endif
</head>

<body>

<div class="main">
    <div class="main-inner">
        <div class="plan-main">
            <main class="Polaris-Frame__Main" id="AppFrameMain" data-has-global-ribbon="false">
                <a id="AppFrameMainContent" tabindex="-1"></a>
                <div class="Polaris-Frame__Content">
                    <div class="Polaris-Page">
                        <div class="Polaris-Page__Content">
                            <div class="Polaris-Layout">

                                <div class="plan-main-inner">

                                    <div class="plan-top-header">

                                        <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">Choose the plan
                                            that’s right for you</h2>
                                        <h6>30 DAY TRIAL ON ALL PLANS</h6>

                                    </div>

                                    <div class="plan-bottom-box">

                                        <div class="row justify-content-center">

                                            <div class="col-lg-6 col-md-6 col-sm-12" style="height: 100%">

                                                <div class="plan-box-main">

                                                    <div class="plan-top-bar">

                                                        <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
                                                            Basic Plan</h2>

                                                    </div>

                                                    <div class="plan-bottom-part">

                                                        <div class="plan-bottom-bar">

                                                            <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
                                                                $10</h2>
                                                            <h5>PER MONTH</h5>

                                                        </div>

                                                        <div class="plan-mid-bar">

                                                            <h5>TRANSACTION FEE: 0.75%</h5>

                                                        </div>

                                                        <div class="plan-discription">

                                                            <ul>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span>GET SETUP IN 5 MINUTES OR LESS</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span>WORKS WITH SHOPIFY PAYMENTS</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span>SUBSCRIBE & SAVE DISCOUNTS</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span>PREPAID SUBSCRIPTIONS</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span>CUSTOMER PORTAL</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span>ADVANCED FAILED PAYMENT RECOVERY</span>
                                                                </li>

                                                            </ul>

                                                        </div>

                                                        <div class="plan-btn-main">
                                                            @if( $plan_id == 2 || !$plan_id)
                                                                <a href="/billing/1" class="Polaris-Button">
																			<span class="Polaris-Button__Content">
																				<span class="Polaris-Button__Text">Start Free Trial</span>
																			</span>
                                                                </a>
                                                            @else
                                                                <button type="button" class="Polaris-Button"
                                                                        style="cursor: none;">
																			<span class="Polaris-Button__Content">
																				<span class="Polaris-Button__Text">Current Plan</span>
																			</span>
                                                                </button>

                                                            @endif
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-12" style="height: 100%">

                                                <div class="plan-box-main">

                                                    <div class="plan-top-bar">

                                                        <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
                                                            Advanced Plan</h2>

                                                    </div>

                                                    <div class="plan-bottom-part">

                                                        <div class="plan-bottom-bar">

                                                            <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
                                                                $50</h2>
                                                            <h5>PER MONTH</h5>

                                                        </div>

                                                        <div class="plan-mid-bar">

                                                            <h5>TRANSACTION FEE: 0.25%</h5>

                                                        </div>

                                                        <div class="plan-discription">

                                                            <ul>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span>REDUCED TRANSACTION FEE</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span>SEND EMAILS FROM CUSTOM DOMAIN</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span>SMS SUBSCRIPTION MANAGEMENT(ADDITIONAL FEE)</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span>SMS UPSELLS (ADDITIONAL FEE)</span>
                                                                </li>

                                                            </ul>

                                                        </div>

                                                        <div class="plan-btn-main">
                                                            @if( $plan_id == 1 || !$plan_id )
                                                                <a href="/billing/2" class="Polaris-Button">
																			<span class="Polaris-Button__Content">
																				<span class="Polaris-Button__Text">Start Free Trial</span>
																			</span>
                                                                </a>
                                                            @else
                                                                <button type="button" class="Polaris-Button"
                                                                        style="cursor: none;">
																			<span class="Polaris-Button__Content">
																				<span class="Polaris-Button__Text">Current Plan</span>
																			</span>
                                                                </button>
                                                            @endif
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="plan-info">

                                <h3>IMPORTANT:<span>YOU WILL BE ASKED TO ACCEPT A CAPPED MONTHLY AMOUNT. tHIS IS REQUIRED FOR US TO CHARGE TRANSACTION FEES</span>
                                </h3>

                            </div>

                        </div>
                    </div>
                </div>
        </main>
    </div>
</div>
</div>

<!-- jQuery and bootstrap css -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<!-- jQuery and bootstrap css end-->
<script type="text/javascript">
    var crawlapps_subscription = {
        init: function () {
            this.addEvent();
        },
        addEvent: function () {
            let url = '{{ route('event')}}';
            $.ajax({
                url: url,
                type: "post",
                data: {'_method': 'post', '_token': "{{ csrf_token() }}", 'category': 'Install', 'description': 'Billing page loaded'},
                success: function (data) {
                    console.log(data);
                },
            });
        },
    };

    $(document).ready(function(){
        crawlapps_subscription.init();
    });
</script>
</body>

</html>
