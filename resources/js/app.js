require('./bootstrap');
window.Vue = require('vue');

import App from './components/layouts';
import VTooltip from 'v-tooltip';

Vue.use(VTooltip)
// import Bugsnag from '@bugsnag/js'
// import BugsnagPluginVue from '@bugsnag/plugin-vue'
//
// Bugsnag.start({
//     apiKey: '4c2f86e8b588a2d19946b9ce731e8c8d',
//     plugins: [new BugsnagPluginVue()]
// })
// Bugsnag.notify(new Error('Test error'))
import router from './routes';
import helper from './helper';
const plugin = {
    install() {
        Vue.prototype.$helpers = helper
    }
}
Vue.use(plugin);

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App),
});
