import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    {
        path:'/',
        component: require('../components/pages/Dashboard').default,
        name:'dashboard',
        meta: {
            title: 'Dashboard',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/subscriber',
        component: require('../components/pages/subscriber/Subscriber').default,
        name:'subscriber',
        meta: {
            title: 'Subscriptions',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/plans',
        component: require('../components/pages/plan/Plans').default,
        name:'plans',
        meta: {
            title: 'Plans',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/billing',
        component: require('../components/pages/plan/Billing').default,
        name:'billing',
        meta: {
            title: 'Billing',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/subscriber-details/:id',
        component: require('../components/pages/subscriber/SubscriberDetail').default,
        name:'subscriber-details',
        meta: {
            title: 'SubscriberDetail',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/subscriber-details-new',
        component: require('../components/pages/subscriber/SubscriberDetailsNew').default,
        name:'subscriber-details-new',
        meta: {
            title: 'SubscriberDetail',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/add-edit-plan/:planId/:planGroupId/:planGroupName',
        component: require('../components/pages/plan/AddEditPlan').default,
        name:'add-edit-plan',
        meta: {
            title: 'AddEditPlan',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/setting',
        component: require('../components/pages/setting/Setting').default,
        name:'setting',
        meta: {
            title: 'Settings',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/help',
        component: require('../components/pages/Help').default,
        name:'help',
        meta: {
            title: 'Help',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },

];

// This callback runs before every route change, including on page load.
const router = new VueRouter({
    mode:'history',
    routes,
    scrollBehavior() {
        return {
            x: 0,
            y: 0,
        };
    },

});
router.afterEach(to => {
    let id = to.params.id;
    let planID = to.params.planId;
    if (typeof id != 'undefined') {
        let nextRoute = to.name;
        let isParam = nextRoute.indexOf("/");

        let url = (isParam >= 0) ? nextRoute.substr(0, (isParam)) : to.name;
        sessionStorage.setItem('LS_ROUTE_KEY', url + '/' + id);
    }
    else if(typeof planID != 'undefined'){
        let planGroupId = to.params.planGroupId;

        let nextRoute = to.name;
        let isParam = nextRoute.indexOf("/");
        let url = (isParam >= 0) ? nextRoute.substr(0, (isParam)) : to.name;
        sessionStorage.setItem('LS_ROUTE_KEY', url + '/' + planID + '/' + planGroupId);
    }else{
        sessionStorage.setItem('LS_ROUTE_KEY', to.name);
    }
    onLoad = 0;
});
router.beforeEach((to, from, next) => {
    const lastRouteName = sessionStorage.getItem('LS_ROUTE_KEY');
    const shouldRedirect = Boolean( lastRouteName && lastRouteName !== 'dashboard' && to.name === 'dashboard');
    if (shouldRedirect) {
        if( onLoad == 1 && lastRouteName != 'dashboard' ){
            let isParam = lastRouteName.indexOf("/");
            if( isParam >= 0 ){
                let param = lastRouteName.substr((isParam + 1));
                let url = lastRouteName.substr(0, (isParam));
                next({name: url, params: { id: param }});
            }else{
                next({name: lastRouteName});
            }
        }
        else if( onLoad == 1 && to.name == 'dashboard' ) next();
        else next();
    }
    else next();
});
let onLoad = 1;
export default router;
