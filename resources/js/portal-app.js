require('./bootstrap');
window.Vue = require('vue');

import VTooltip from 'v-tooltip';

Vue.use(VTooltip)
import helper from './helper';
const plugin = {
    install() {
        Vue.prototype.$helpers = helper
    }
}
Vue.use(plugin);

Vue.component('index', require('./components/pages/portal/index.vue').default);

const appPortal = new Vue({
    el: '#portal'
});
