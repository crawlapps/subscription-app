/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/portal.js":
/*!********************************!*\
  !*** ./resources/js/portal.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var host = "https://subscription-app.test";
var apiEndPoint = host + '/api';
var self = "";
var crawlapps_simplee = {
  init: function init() {
    this.subscription();
  },
  subscription: function subscription(id, type) {
    $(document).on('click', '.contract_status', function () {
      var id = $(this).data('id');
      var custid = $(this).data('custid');
      var type = $(this).data('type');
      var shopifyDomain = $(this).data('shop');
      var data = {
        contract_id: id,
        type: type,
        customer_id: custid,
        shop: shopifyDomain,
        mode: 'api',
        ship_add1: $('#new_ship_add').val()
      };
      var aPIEndPoint = "".concat(apiEndPoint, "/subscriber");
      console.log(aPIEndPoint);
      $.ajax({
        method: "post",
        url: aPIEndPoint,
        data: {
          data: data
        },
        success: function success(response, _success, header) {
          console.log(response); // if( response.data ){

          window.location.reload(); // }
        }
      });
    });
    $('#edit-ship_add').on('click', function () {
      var custid = $(this).data('custid');
      var contract_id = $(this).data('id');
      $('#ship_add_save').data('custid', custid);
      $('#ship_add_save').data('id', contract_id);
      var add = $(this).data('add');
      $('#new_ship_add').val(add);
    });
  }
};
$(document).ready(function () {
  crawlapps_simplee.init();
});

/***/ }),

/***/ 1:
/*!**************************************!*\
  !*** multi ./resources/js/portal.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/shopify/subscription-app/resources/js/portal.js */"./resources/js/portal.js");


/***/ })

/******/ });